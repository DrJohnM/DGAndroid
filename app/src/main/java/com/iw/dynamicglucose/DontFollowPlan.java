package com.iw.dynamicglucose;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.iw.dynamicglucose.ExtraObjects.DataDontFollow;
import com.iw.dynamicglucose.config.Service;
import com.iw.dynamicglucose.config.Session;
import com.iw.dynamicglucose.services.DailyWorkout.DailyWorkouts;
import com.iw.dynamicglucose.services.Services;
import com.iw.dynamicglucose.services.applogin.UserSession;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DontFollowPlan extends AppCompatActivity {
    private Services services;
    private UserSession user;
    private EditText input;
    private MaterialDialog dialogLifestyle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dont_follow_plan);
        getSupportActionBar().hide();

        Service service = new Service();
        services = service.getService();
        Session session = new Session(getBaseContext());
        user = session.getSession();

        Button send = findViewById(R.id.btn_dont_follow);
        Button skip = findViewById(R.id.btn_skip_dont_follow);
        ImageButton close = findViewById(R.id.closeButtonCloseFinish);
        ImageButton back = findViewById(R.id.backButtonBackFinish);
        input = findViewById(R.id.input_dont_follow);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), Home.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(i);
                finish();
            }
        });

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(input.getText().toString().equals("")){
                    createToast("TextField cannot be empty");
                }else{
                    set(input.getText().toString());
                }
            }
        });

        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadWorkoutse();
            }
        });
    }
    private void loadWorkoutse(){
        // Create the call of the service view program progress
        Call<DailyWorkouts> call = services.get_daily_workouts(user.getId());

        // Executing call of the service view program progress
        call.enqueue(new Callback<DailyWorkouts>() {
            @Override
            public void onResponse(Call<DailyWorkouts> call, Response<DailyWorkouts> response) {
                switch (response.code()) {
                    case 200:
                        Intent i = new Intent(getApplicationContext(), DailyCheckWorkouts.class);
                        startActivity(i);
                        break;
                    case 204:
                        Intent j = new Intent(getApplicationContext(), DailyCheckoutGral.class);
                        startActivity(j);

                        break;
                    default:

                        break;
                }
            }

            @Override
            public void onFailure(Call<DailyWorkouts> call, Throwable t) {
                createToast("Error: " + t.getMessage());
            }
        });
    }
    private void  set(String text){
        dialogLifestyle = loader();
        dialogLifestyle.show();
        // Create the call of the service view program progress
        DataDontFollow data = new DataDontFollow(user.getId(), text);
        Call<ResponseBody> call = services.set_daily_dont_follow(data);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                switch (response.code()) {
                    case 200:
                       loadWorkoutse();
                        break;
                    default:
                        break;
                }
                dialogLifestyle.dismiss();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                createToast("Error" + t);
                dialogLifestyle.dismiss();
            }
        });
    }
    private void createToast(String msg){
        Toast toast = Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }
    private MaterialDialog loader(){
        MaterialDialog.Builder builder = new MaterialDialog.Builder(this)
                .content("Please wait...")
                .progress(true, 0);
        return builder.build();
    }
}
