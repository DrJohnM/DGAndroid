package com.iw.dynamicglucose;

import android.content.Context;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.iw.dynamicglucose.services.get_day_meals.GetDayMeal;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by anavictoriafrias on 31/01/18.
 */

public class AdapterHomeMeals extends RecyclerView.Adapter<AdapterHomeMeals.ViewHolder> {

    private ArrayList<GetDayMeal> getDayMeals;
    private Context context;
    private BottomNavigationView bottomNavigationView;

    AdapterHomeMeals(Context context, ArrayList<GetDayMeal> getDayMeals, BottomNavigationView bottomNavigationView) {
        this.getDayMeals = getDayMeals;
        this.context = context;
        this.bottomNavigationView = bottomNavigationView;
    }


    @Override
    public AdapterHomeMeals.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_meals_home, parent, false));
    }

    @Override
    public void onBindViewHolder(AdapterHomeMeals.ViewHolder holder, int position) {
        final GetDayMeal getDayMeal = getDayMeals.get(position);
        holder.time.setText(getDayMeal.getMealTime());
        holder.meals.setText(getDayMeal.getMealTimeName());
        Picasso.with(context).load(getDayMeal.getImage()).into(holder.imageMeal);
        holder.content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MenuItem item = bottomNavigationView.getMenu().getItem(1);
                item.setChecked(true);
                fragment_meals fragment = fragment_meals.newInstance();
                FragmentTransaction ft = ((FragmentActivity)context).getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.frame_layout, fragment);
                ft.commit();
            }
        });
        holder.see_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MenuItem item = bottomNavigationView.getMenu().getItem(1);
                item.setChecked(true);
                fragment_meals fragment = fragment_meals.newInstance();
                FragmentTransaction ft = ((FragmentActivity)context).getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.frame_layout, fragment);
                ft.commit();
            }
        });
        holder.currentMeal.setText(getDayMeal.getMealTimeNameTime());
    }

    @Override
    public int getItemCount() {
        return getDayMeals.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder{

        RelativeLayout content;
        ImageView imageMeal;
        TextView currentMeal;
        TextView time;
        TextView meals;
        TextView see_all;

        ViewHolder(View itemView) {
            super(itemView);
            content = itemView.findViewById(R.id.lay_content);
            imageMeal = itemView.findViewById(R.id.image_meal);
            currentMeal = itemView.findViewById(R.id.current);
            time = itemView.findViewById(R.id.time_meal);
            meals = itemView.findViewById(R.id.meals);
            see_all = itemView.findViewById(R.id.see_all);
        }
    }
}
