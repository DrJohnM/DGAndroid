package com.iw.dynamicglucose;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.iw.dynamicglucose.config.Service;
import com.iw.dynamicglucose.config.Session;
import com.iw.dynamicglucose.services.Services;
import com.iw.dynamicglucose.services.applogin.UserSession;
import com.iw.dynamicglucose.services.view_progress_program.ViewProgramProgress;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Members extends AppCompatActivity {


    private Services services;
    private Session session;
    private UserSession user;
    private TextView txtDays;

    // Declare other vars
    private MaterialDialog loader;
    private MaterialDialog dialogDays;

    // Declare vars layout
    private ListView lista;
    private ImageButton backButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_members);
        getSupportActionBar().hide();

        Service service = new Service();
        services = service.getService();
        session = new Session(getBaseContext());
        user = session.getSession();
        lista = findViewById(R.id.list_members);
        txtDays = findViewById(R.id.lbl_days);
        backButton = findViewById(R.id.backButton_members);

        setListeners();

        loadMembers();
        loadProgramProgress();
    }

    private void setListeners() {
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void loadMembers(){
        loader = loader();
        loader.show();
        // Create the call of the service view program progress
        Call<com.iw.dynamicglucose.services.Members.Members> call = services.get_members(user.getId().toString());

        // Executing call of the service view program progress
        call.enqueue(new Callback<com.iw.dynamicglucose.services.Members.Members>() {
            @Override
            public void onResponse(Call<com.iw.dynamicglucose.services.Members.Members> call, Response<com.iw.dynamicglucose.services.Members.Members> response) {
                switch (response.code()){
                    case 200:
                        com.iw.dynamicglucose.services.Members.Members data = response.body();
                        lista.setAdapter(new AdapterMembers(getApplicationContext(), data));

                        break;
                    default:
                        MaterialDialog.Builder dialog = createMessage();
                        dialog.build();
                        dialog.show();
                        break;
                }
                loader.dismiss();
            }

            @Override
            public void onFailure(Call<com.iw.dynamicglucose.services.Members.Members> call, Throwable t) {
                loader.dismiss();
                MaterialDialog.Builder dialog = createMessage();
                dialog.build();
                dialog.show();
            }

        });
    }

    private void loadProgramProgress(){
        dialogDays = loader();
        dialogDays.show();
        // Create the call of the service view program progress
        Call<ViewProgramProgress> call = services.view_program_progress(user.getId());

        // Executing call of the service view program progress
        call.enqueue(new Callback<ViewProgramProgress>() {
            @Override
            public void onResponse(Call<ViewProgramProgress> call, Response<ViewProgramProgress> response) {
                switch (response.code()){
                    case 200:
                        ViewProgramProgress data = response.body();
                        assert data != null;
                        if(data.getIs_active()){
                            txtDays.setText(data.getProgress_program());
                        }else{
                            session.CloseSession();
                            Intent i = new Intent(getApplicationContext(), Login.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            startActivity(i);
                            finish();
                        }

                        break;
                    default:
                        MaterialDialog.Builder dialog = createMessage();
                        dialog.build();
                        dialog.show();
                        break;
                }
                dialogDays.dismiss();
            }

            @Override
            public void onFailure(Call<ViewProgramProgress> call, Throwable t) {
                dialogDays.dismiss();
                MaterialDialog.Builder dialog = createMessage();
                dialog.build();
                dialog.show();
            }
        });
    }


    private MaterialDialog.Builder createMessage(){
        return new MaterialDialog.Builder(this)
                .title("Error")
                .content("Server not available, try again later")
                .positiveText("Accept");
    }

    private MaterialDialog loader(){
        MaterialDialog.Builder builder = new MaterialDialog.Builder(this)
                .content("Please wait...")
                .progress(true, 0);
        return builder.build();
    }
}
