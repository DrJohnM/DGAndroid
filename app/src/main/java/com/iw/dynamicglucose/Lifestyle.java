package com.iw.dynamicglucose;

import android.annotation.SuppressLint;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.iw.dynamicglucose.ExtraObjects.DataLifestyle;
import com.iw.dynamicglucose.config.Service;
import com.iw.dynamicglucose.config.Session;
import com.iw.dynamicglucose.services.Services;
import com.iw.dynamicglucose.services.applogin.UserSession;
import com.iw.dynamicglucose.services.get_lifestyle.GetLifestyle;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Lifestyle extends AppCompatActivity {
    private Services services;
    private UserSession user;
    private ListView lista;
    private TextView question;
    private TextView count;
    private Integer counter = 0;
    private Integer position = null;
    private ArrayList<GetLifestyle> questions;
    private ArrayList<Long> answerids = new ArrayList<>();
    private Boolean flagSettings = false;
    private MaterialDialog dialogLifestyle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lifestyle);

        getSupportActionBar().hide();
        flagSettings = getIntent().getBooleanExtra("settings", false);

        Service service = new Service();
        services = service.getService();
        Session session = new Session(getBaseContext());
        user = session.getSession();
        count = findViewById(R.id.txt_title_lifestyle);
        question = findViewById(R.id.txt_count_lifestyle);
        Button button = findViewById(R.id.btn_lifestyle);
        lista = findViewById(R.id.list_lifestyle);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    if(position != null){
                        answerids.add(lista.getAdapter().getItemId(position));
                        position = null;
                        if (counter == questions.size()-1){
                            DataLifestyle data = new DataLifestyle(user.getId(), 9, answerids);
                            setLifstyle(data);
                        }else{
                            counter++;
                            chargeQuestion(counter);
                        }
                    }else {
                        createToast("Please select an item");
                    }
            }
        });
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                position = i;
            }
        });
        loadLifestyle();
    }

    private void loadLifestyle(){
        dialogLifestyle = loader();
        dialogLifestyle.show();
        // Create the call of the service view program progress
        Call<ArrayList<GetLifestyle>> call = services.get_lifestyle();

        // Executing call of the service view program progress
        call.enqueue(new Callback<ArrayList<GetLifestyle>>() {
            @Override
            public void onResponse(Call<ArrayList<GetLifestyle>> call, Response<ArrayList<GetLifestyle>> response) {
                switch (response.code()) {
                    case 200:
                        questions  = response.body();
                        chargeQuestion(counter);
                        break;
                    default:

                        break;
                }
                dialogLifestyle.dismiss();
            }

            @Override
            public void onFailure(Call<ArrayList<GetLifestyle>> call, Throwable t) {
                //progressBar.setVisibility(View.GONE);
                dialogLifestyle.dismiss();
                createToast("Error: " + t.getMessage());
            }
        });
    }
    private void  setLifstyle(DataLifestyle info){
        dialogLifestyle = loader();
        dialogLifestyle.show();
        // Create the call of the service view program progress
        Call<ResponseBody> call = services.set_lifestyle_answers(info);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialogLifestyle.hide();
                switch (response.code()) {
                    case 200:
                        if(flagSettings){
                            Intent i = new Intent(getApplicationContext(), LifestyleVideo.class);
                            i.putExtra("settings", true);
                            startActivity(i);
                            finish();
                        }else{
                            Intent i = new Intent(getApplicationContext(), LifestyleQuestionWarning.class);
                            startActivity(i);
                            finish();
                        }
                        break;
                    default:
                        MaterialDialog.Builder dialog = createMessage();
                        dialog.build();
                        dialog.show();
                        break;
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialogLifestyle.hide();
                createToast("Error" + t);
            }
        });
    }
    @SuppressLint("SetTextI18n")
    private void chargeQuestion(Integer counter){
        count.setText((counter +1) + " of " + (questions.size() + 1));
        question.setText(questions.get(counter).getQuestion());
        lista.setAdapter(new AdapterLifestyle(getApplicationContext(), questions.get(counter).getAnswers(), counter, questions.size()));
    }
    private void createToast(String msg){
        Toast toast = Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    private MaterialDialog loader(){
        MaterialDialog.Builder builder = new MaterialDialog.Builder(this)
                .content("Please wait...")
                .progress(true, 0);
        return builder.build();
    }
    private MaterialDialog.Builder createMessage(){
        return new MaterialDialog.Builder(getApplicationContext())
                .title("Error")
                .content("Server not available, try again later")
                .positiveText("Accept");
    }
}
