package com.iw.dynamicglucose.config;

import com.iw.dynamicglucose.Getting;
import com.iw.dynamicglucose.OnboardingFruits;
import com.iw.dynamicglucose.OnboardingMeats;
import com.iw.dynamicglucose.OnboardingProducts;
import com.iw.dynamicglucose.OnboardingVegetables;
import com.iw.dynamicglucose.PrevFoodSelection;

/**
 * Created by IW on 20/03/2018.
 */

public class OnboardingPositions {
    private Object [] array = new Object[6];

    public OnboardingPositions() {
        array[0] = Getting.class;
        array[1] = PrevFoodSelection.class;
        array[2] = OnboardingVegetables.class;
        array[3] = OnboardingFruits.class;
        array[4] = OnboardingProducts.class;
    }

    public Object[] getArray() {
        return array;
    }
}
