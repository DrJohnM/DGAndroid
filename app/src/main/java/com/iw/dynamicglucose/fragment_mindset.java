package com.iw.dynamicglucose;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.halilibo.bvpkotlin.*;
import com.iw.dynamicglucose.config.BaseUrl;
import com.iw.dynamicglucose.config.Service;
import com.iw.dynamicglucose.config.Session;
import com.iw.dynamicglucose.services.Services;
import com.iw.dynamicglucose.services.applogin.UserSession;
import com.iw.dynamicglucose.services.today_mindset.TodayMindset;
import com.iw.dynamicglucose.services.today_process_mindset.TodayProcessMindset;
import com.iw.dynamicglucose.services.view_progress_program.ViewProgramProgress;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class fragment_mindset extends Fragment implements VideoCallback {

    // Declare vars config
    private BaseUrl baseUrl;
    private Services services;
    private Session session;
    private UserSession user;

    //private VideoView videoView;
    private Button markDone;
    private TextView title;
    private TextView time;
    private ImageButton rate1;
    private ImageButton rate2;
    private ImageButton rate3;
    private ImageButton rate4;
    private ImageButton rate5;
    private TextView lblrate;
    private TextView content;
    private RelativeLayout showMindset;
    private RelativeLayout hiddenMindset;
    private TextView txtDays;
    private Integer mindset_id;
    private TextView btn_see_more;
    private Boolean buttonSet;
    private Call doneCall;
    private int _rating = 0;

    // Declare var loader
    private MaterialDialog loader;
    private  MaterialDialog dialogDays;
    private BetterVideoPlayer player;
    private ArrayList<Call<? extends  Object>> calls = new ArrayList<>();

    // TODO: Rename parameter arguments, choose names that match
    public static fragment_mindset newInstance() {
        return new fragment_mindset();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_fragment_mindset, container, false);
    }
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //Llamada de variables
        //videoView = getView().findViewById(R.id.video_container);
        markDone = getView().findViewById(R.id.btn_markDoneMindset);
        title = getView().findViewById(R.id.txt_title_mindset);
        time = getView().findViewById(R.id.txt_time_mindset);
        rate1 = getView().findViewById(R.id.btn_rate_1_mindset);
        rate2 = getView().findViewById(R.id.btn_rate_2_mindset);
        rate3 = getView().findViewById(R.id.btn_rate_3_mindset);
        rate4 = getView().findViewById(R.id.btn_rate_4_mindset);
        rate5 = getView().findViewById(R.id.btn_rate_5_mindset);
        lblrate = getView().findViewById(R.id.txt_rate_mindset);
        content = getView().findViewById(R.id.txt_content_mindset);
        showMindset = getView().findViewById(R.id.show_mindset);
        hiddenMindset = getView().findViewById(R.id.hidden_mindset);
        txtDays = getView().findViewById(R.id.lbl_days);
        btn_see_more = getView().findViewById(R.id.btn_see_more);
        player = getView().findViewById(R.id.player);
        player.setCallback(fragment_mindset.this);
        player.setAutoPlay(true);

        // Assignment vars config
        Service service = new Service();
        services = service.getService();
        session = new Session(getContext());
        user = session.getSession();

        if(session.getCodeNotif() != 0){
            session.setCodeNotif(0);
        }
        rate1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rate(user.getId(), mindset_id, 1);
            }
        });
        rate2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rate(user.getId(), mindset_id, 2);
            }
        });
        rate3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rate(user.getId(), mindset_id, 3);
            }
        });
        rate4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rate(user.getId(), mindset_id, 4);
            }
        });
        rate5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rate(user.getId(), mindset_id, 5);
            }
        });
        markDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                markAsDone(user.getId(), mindset_id);
            }
        });
        loadMindset();
        loadProgramProgress();
        btn_see_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment selectedFragment = null;
                selectedFragment = MindsetList.newInstance();
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_layout, selectedFragment);
                transaction.commit();
            }
        });
    }
    private void loadMindset(){
        loader = loader();
        loader.show();

        // Create the call of the service time to start
        Call<ArrayList<TodayMindset>> call = services.today_mindset(user.getId());

        // Executing call of the service time to start
        call.enqueue(new Callback<ArrayList<TodayMindset>>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<ArrayList<TodayMindset>> call, Response<ArrayList<TodayMindset>> response) {
                switch (response.code()){
                    case 200:
                        // Getting data objects
                        ArrayList<TodayMindset> data = response.body();
                        if(session.getSession().getShow_mindsets()){
                            assert data != null;
                            loadProcessMindset(data.get(0).getId());
                        }else{
                            showMindset.setVisibility(View.GONE);
                            hiddenMindset.setVisibility(View.VISIBLE);
                        }
                        mindset_id = data.get(0).getId();
                        break;
                    case 204:
                        showMindset.setVisibility(View.GONE);
                        hiddenMindset.setVisibility(View.VISIBLE);
                        break;
                    default:
                        MaterialDialog.Builder dialog = createMessage();
                        dialog.build();
                        dialog.show();
                        break;
                }
                loader.dismiss();
            }

            @Override
            public void onFailure(Call<ArrayList<TodayMindset>> call, Throwable t) {
                loader.dismiss();
                MaterialDialog.Builder dialog = createMessage();
                if (dialog != null){
                    dialog.build();
                    dialog.show();
                }
                calls.remove(call);

            }


        });
        calls.add(call);


    }

    private void setButton(boolean done){
        if (done){
            markDone.setBackground(getResources().getDrawable(R.drawable.green_selected_rounded_button));
            markDone.setTextColor(Color.WHITE);
            markDone.setText(getString(R.string.done));

        }else {
            markDone.setBackground(getResources().getDrawable(R.drawable.green_rounded_button));
            markDone.setTextColor(getResources().getColor(R.color.purpleGray));
            markDone.setText(getString(R.string.markAsDone));
        }
        buttonSet = done;
    }

    private void setRate(Integer rating) {
        _rating = rating;
        ImageButton[] rates = {rate1, rate2, rate3, rate4, rate5};
        for (ImageButton rate: rates) {
            rate.setImageResource(R.drawable.star);
        }
        for (int i=0; i<_rating; i++){
            rates[i].setImageResource(R.drawable.star_checked);
        }
    }
    private void rate(Integer patient_id, Integer mindset_id, final Integer rating){
        final int oldRate = _rating;
        setRate(rating);

        // Create the call of the service time to start
        Call<ResponseBody> call = services.set_rating_mindset(patient_id,mindset_id,rating);

        // Executing call of the service time to start
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() != 200) {
                    setRate(oldRate);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                MaterialDialog.Builder dialog = createMessage();
                if (dialog != null){
                    dialog.build();
                    dialog.show();
                }
                calls.remove(call);
                setRate(oldRate);
            }
        });
        calls.add(call);

        loader.dismiss();
    }
    private void loadProcessMindset(Integer id_mindset){
        // Create the call of the service time to start
        Call<TodayProcessMindset> call = services.today_process_mindset(id_mindset, user.getId());

        // Executing call of the service time to start
        call.enqueue(new Callback<TodayProcessMindset>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<TodayProcessMindset> call, Response<TodayProcessMindset> response) {
                switch (response.code()){
                    case 200:
                        // Getting data objects
                        TodayProcessMindset data = response.body();
                        assert data != null;
                        setButton(data.getDone());
                        player.setSource(Uri.parse(data.getMedia()));
                        title.setText(data.getTitle());
                        time.setText(data.getDuration());
                        content.setText(data.getDescription());
                        lblrate.setText(data.getRating().toString() + "/5");
                        setRate(data.getRating());
                        break;

                    case 204:
                        showMindset.setVisibility(View.GONE);
                        hiddenMindset.setVisibility(View.VISIBLE);
                        break;

                    default:
                        MaterialDialog.Builder dialog = createMessage();
                        dialog.build();
                        dialog.show();
                        break;
                }
                loader.dismiss();
            }

            @Override
            public void onFailure(Call<TodayProcessMindset> call, Throwable t) {
                Log.e("Error", "call: " + t.getMessage());
                loader.dismiss();
                MaterialDialog.Builder dialog = createMessage();
                if (dialog != null) {
                    dialog.build();
                    dialog.show();
                }
                calls.remove(call);

            }


        });
        calls.add(call);

    }
    private void markAsDone(Integer patient_id, Integer mindset_id){
        final Boolean previous = buttonSet;
        setButton(!previous);

        // Create the call of the service time to start
        Call<ResponseBody> call = services.set_progress_mindset(patient_id,mindset_id, buttonSet?"true":"false");
        if(doneCall != null){
            doneCall.cancel();
        }
        doneCall = call;

        // Executing call of the service time to start
        call.enqueue(new Callback<ResponseBody>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() != 200) {
                    setButton(previous);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (!call.isCanceled()) {
                    setButton(previous);
                    Log.e("errorCall", t.getMessage());
                }
            }
        });
        calls.add(call);

    }
    private void loadProgramProgress(){
        dialogDays = loader();
        dialogDays.show();
        // Create the call of the service view program progress
        Call<ViewProgramProgress> call = services.view_program_progress(user.getId());

        // Executing call of the service view program progress
        call.enqueue(new Callback<ViewProgramProgress>() {
            @Override
            public void onResponse(Call<ViewProgramProgress> call, Response<ViewProgramProgress> response) {
                switch (response.code()){
                    case 200:
                        ViewProgramProgress data = response.body();
                        assert data != null;
                        if(data.getIs_active()){
                            txtDays.setText(data.getProgress_program());
                        }else{
                            session.CloseSession();
                            Intent i = new Intent(getContext(), Login.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            startActivity(i);
                            getActivity().finish();
                        }

                        break;
                    default:
                        MaterialDialog.Builder dialog = createMessage();
                        dialog.build();
                        dialog.show();
                        break;
                }
                dialogDays.dismiss();
            }

            @Override
            public void onFailure(Call<ViewProgramProgress> call, Throwable t) {
                dialogDays.dismiss();
                MaterialDialog.Builder dialog = createMessage();
                if (dialog != null){
                    dialog.build();
                    dialog.show();
                }
                calls.remove(call);
            }
        });
        calls.add(call);

    }

    private MaterialDialog.Builder createMessage(){
        Context context = getContext();
        if (context == null) {
            return null;
        }

        return new MaterialDialog.Builder(context)
                .title("Error")
                .content("Server not available, try again later")
                .positiveText("Accept");
    }

    private MaterialDialog loader(){
        Context context = getContext();
        if (context == null) {
            return null;
        }
        MaterialDialog.Builder builder = new MaterialDialog.Builder(context)
                .content("Please wait...")
                .progress(true, 0);
        return builder.build();
    }

    @Override
    public void onPause() {
        super.onPause();
        player.pause();
    }

    @Override
    public void onStarted(BetterVideoPlayer player) {

    }

    @Override
    public void onPaused(BetterVideoPlayer player) {

    }

    @Override
    public void onPreparing(BetterVideoPlayer player) {

    }

    @Override
    public void onPrepared(BetterVideoPlayer player) {
        player.start();
    }

    @Override
    public void onBuffering(int percent) {

    }

    @Override
    public void onError(BetterVideoPlayer player, Exception e) {

    }

    @Override
    public void onCompletion(BetterVideoPlayer player) {

    }

    @Override
    public void onToggleControls(BetterVideoPlayer player, boolean isShowing) {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        //cancel requests that are still executing
        for (Call call : calls) {
            call.cancel();
        }
    }

}
