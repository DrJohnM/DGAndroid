package com.iw.dynamicglucose;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.iw.dynamicglucose.ExtraObjects.DataFeeling;
import com.iw.dynamicglucose.config.Service;
import com.iw.dynamicglucose.config.Session;
import com.iw.dynamicglucose.services.Services;
import com.iw.dynamicglucose.services.applogin.UserSession;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FinishCheck extends AppCompatActivity {
    private Services services;
    private UserSession user;
    private ImageButton f1;
    private ImageButton f2;
    private ImageButton f3;
    private ImageButton f4;
    private ImageButton f5;
    private ArrayList<Boolean> flags = new ArrayList<Boolean>();
    private MaterialDialog dialogLifestyle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finish_check);
        getSupportActionBar().hide();

        Service service = new Service();
        services = service.getService();
        Session session = new Session(getBaseContext());
        user = session.getSession();
        Button next = findViewById(R.id.btn_finish_check);
        ImageButton close = findViewById(R.id.closeButtonCloseFinish);
        ImageButton back = findViewById(R.id.backButtonBackFinish);
        f1 = findViewById(R.id.feeling1);
        f2 = findViewById(R.id.feeling2);
        f3 = findViewById(R.id.feeling3);
        f4 = findViewById(R.id.feeling4);
        f5 = findViewById(R.id.feeling5);
        flags.add(false);
        flags.add(false);
        flags.add(false);
        flags.add(false);
        flags.add(false);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for(int i = 0 ; i<flags.size(); i++){
                    if (flags.get(i)){
                        setinish(i+1);
                    }
                }
                if(!flags.contains(true)){
                    createToast("Please select one feeling");
                }
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), Home.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(i);
                finish();
            }
        });
        f1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                android.view.ViewGroup.LayoutParams paramsBig1 = f1.getLayoutParams();
                android.view.ViewGroup.LayoutParams paramsSmall2 = f2.getLayoutParams();
                android.view.ViewGroup.LayoutParams paramsSmall3 = f3.getLayoutParams();
                android.view.ViewGroup.LayoutParams paramsSmall4 = f4.getLayoutParams();
                android.view.ViewGroup.LayoutParams paramsSmall5 = f5.getLayoutParams();
                paramsSmall2.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 35, getResources().getDisplayMetrics());
                paramsSmall2.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 35, getResources().getDisplayMetrics());
                paramsSmall3.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 35, getResources().getDisplayMetrics());
                paramsSmall3.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 35, getResources().getDisplayMetrics());
                paramsSmall4.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 35, getResources().getDisplayMetrics());
                paramsSmall4.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 35, getResources().getDisplayMetrics());
                paramsSmall5.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 35, getResources().getDisplayMetrics());
                paramsSmall5.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 35, getResources().getDisplayMetrics());
                paramsBig1.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 42, getResources().getDisplayMetrics());
                paramsBig1.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 42, getResources().getDisplayMetrics());
                flags.set(0, true);
                flags.set(1, false);
                flags.set(2, false);
                flags.set(3, false);
                flags.set(4, false);
                f1.setLayoutParams(paramsBig1);
                f2.setLayoutParams(paramsSmall2);
                f3.setLayoutParams(paramsSmall3);
                f4.setLayoutParams(paramsSmall4);
                f5.setLayoutParams(paramsSmall5);
            }
        });
        f2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                android.view.ViewGroup.LayoutParams paramsSmall1 = f1.getLayoutParams();
                android.view.ViewGroup.LayoutParams paramsBig2 = f2.getLayoutParams();
                android.view.ViewGroup.LayoutParams paramsSmall3 = f3.getLayoutParams();
                android.view.ViewGroup.LayoutParams paramsSmall4 = f4.getLayoutParams();
                android.view.ViewGroup.LayoutParams paramsSmall5 = f5.getLayoutParams();
                paramsSmall1.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 35, getResources().getDisplayMetrics());
                paramsSmall1.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 35, getResources().getDisplayMetrics());
                paramsSmall3.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 35, getResources().getDisplayMetrics());
                paramsSmall3.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 35, getResources().getDisplayMetrics());
                paramsSmall4.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 35, getResources().getDisplayMetrics());
                paramsSmall4.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 35, getResources().getDisplayMetrics());
                paramsSmall5.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 35, getResources().getDisplayMetrics());
                paramsSmall5.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 35, getResources().getDisplayMetrics());
                paramsBig2.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 42, getResources().getDisplayMetrics());
                paramsBig2.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 42, getResources().getDisplayMetrics());
                flags.set(0, false);
                flags.set(1, true);
                flags.set(2, false);
                flags.set(3, false);
                flags.set(4, false);
                f1.setLayoutParams(paramsSmall1);
                f2.setLayoutParams(paramsBig2);
                f3.setLayoutParams(paramsSmall3);
                f4.setLayoutParams(paramsSmall4);
                f5.setLayoutParams(paramsSmall5);
            }
        });
        f3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                android.view.ViewGroup.LayoutParams paramsSmall1 = f1.getLayoutParams();
                android.view.ViewGroup.LayoutParams paramsSmall2 = f2.getLayoutParams();
                android.view.ViewGroup.LayoutParams paramsBig3 = f3.getLayoutParams();
                android.view.ViewGroup.LayoutParams paramsSmall4 = f4.getLayoutParams();
                android.view.ViewGroup.LayoutParams paramsSmall5 = f5.getLayoutParams();
                paramsSmall2.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 35, getResources().getDisplayMetrics());
                paramsSmall2.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 35, getResources().getDisplayMetrics());
                paramsSmall1.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 35, getResources().getDisplayMetrics());
                paramsSmall1.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 35, getResources().getDisplayMetrics());
                paramsSmall4.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 35, getResources().getDisplayMetrics());
                paramsSmall4.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 35, getResources().getDisplayMetrics());
                paramsSmall5.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 35, getResources().getDisplayMetrics());
                paramsSmall5.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 35, getResources().getDisplayMetrics());
                paramsBig3.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 42, getResources().getDisplayMetrics());
                paramsBig3.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 42, getResources().getDisplayMetrics());
                flags.set(0, false);
                flags.set(1, false);
                flags.set(2, true);
                flags.set(3, false);
                flags.set(4, false);
                f1.setLayoutParams(paramsSmall1);
                f2.setLayoutParams(paramsSmall2);
                f3.setLayoutParams(paramsBig3);
                f4.setLayoutParams(paramsSmall4);
                f5.setLayoutParams(paramsSmall5);
            }
        });
        f4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                android.view.ViewGroup.LayoutParams paramsSmall1 = f1.getLayoutParams();
                android.view.ViewGroup.LayoutParams paramsSmall2 = f2.getLayoutParams();
                android.view.ViewGroup.LayoutParams paramsSmall3 = f3.getLayoutParams();
                android.view.ViewGroup.LayoutParams paramsBig4 = f4.getLayoutParams();
                android.view.ViewGroup.LayoutParams paramsSmall5 = f5.getLayoutParams();
                paramsSmall2.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 35, getResources().getDisplayMetrics());
                paramsSmall2.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 35, getResources().getDisplayMetrics());
                paramsSmall3.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 35, getResources().getDisplayMetrics());
                paramsSmall3.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 35, getResources().getDisplayMetrics());
                paramsSmall1.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 35, getResources().getDisplayMetrics());
                paramsSmall1.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 35, getResources().getDisplayMetrics());
                paramsSmall5.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 35, getResources().getDisplayMetrics());
                paramsSmall5.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 35, getResources().getDisplayMetrics());
                paramsBig4.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 42, getResources().getDisplayMetrics());
                paramsBig4.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 42, getResources().getDisplayMetrics());
                flags.set(0, false);
                flags.set(1, false);
                flags.set(2, false);
                flags.set(3, true);
                flags.set(4, false);
                f1.setLayoutParams(paramsSmall1);
                f2.setLayoutParams(paramsSmall2);
                f3.setLayoutParams(paramsSmall3);
                f4.setLayoutParams(paramsBig4);
                f5.setLayoutParams(paramsSmall5);
            }
        });
        f5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                android.view.ViewGroup.LayoutParams paramsSmall1 = f1.getLayoutParams();
                android.view.ViewGroup.LayoutParams paramsSmall2 = f2.getLayoutParams();
                android.view.ViewGroup.LayoutParams paramsSmall3 = f3.getLayoutParams();
                android.view.ViewGroup.LayoutParams paramsSmall4 = f4.getLayoutParams();
                android.view.ViewGroup.LayoutParams paramsBig5 = f5.getLayoutParams();
                paramsSmall2.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 35, getResources().getDisplayMetrics());
                paramsSmall2.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 35, getResources().getDisplayMetrics());
                paramsSmall3.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 35, getResources().getDisplayMetrics());
                paramsSmall3.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 35, getResources().getDisplayMetrics());
                paramsSmall4.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 35, getResources().getDisplayMetrics());
                paramsSmall4.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 35, getResources().getDisplayMetrics());
                paramsSmall1.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 35, getResources().getDisplayMetrics());
                paramsSmall1.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 35, getResources().getDisplayMetrics());
                paramsBig5.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 42, getResources().getDisplayMetrics());
                paramsBig5.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 42, getResources().getDisplayMetrics());
                flags.set(0, false);
                flags.set(1, false);
                flags.set(2, false);
                flags.set(3, false);
                flags.set(4, true);
                f1.setLayoutParams(paramsSmall1);
                f2.setLayoutParams(paramsSmall2);
                f3.setLayoutParams(paramsSmall3);
                f4.setLayoutParams(paramsSmall4);
                f5.setLayoutParams(paramsBig5);
            }
        });
    }
    private void  setinish(Integer feeling){
        dialogLifestyle = loader();
        dialogLifestyle.show();
        // Create the call of the service view program progress
        DataFeeling data = new DataFeeling(user.getId(), feeling);
        Call<ResponseBody> call = services.set_daily_feeling(data);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                switch (response.code()) {
                    case 200:
                        Intent i = new Intent(getApplicationContext(), Home.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(i);
                        finish();
                        break;
                    default:
                        break;
                }
                dialogLifestyle.dismiss();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                createToast("Error" + t);
                dialogLifestyle.dismiss();
            }
        });
    }
    private void createToast(String msg){
        Toast toast = Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }
    private MaterialDialog loader(){
        MaterialDialog.Builder builder = new MaterialDialog.Builder(this)
                .content("Please wait...")
                .progress(true, 0);
        return builder.build();
    }
}
