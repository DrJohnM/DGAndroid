package com.iw.dynamicglucose.ExtraObjects;

import android.util.Log;

import com.iw.dynamicglucose.config.Service;
import com.iw.dynamicglucose.services.Services;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VideoProgressCallbackReporter implements com.halilibo.bvpkotlin.VideoProgressCallback {
    private  Double videoWatchedDuration;
    private  Double lastTime;
    private Services services;
    private  Integer stepId;
    private  Integer patientId;

    public VideoProgressCallbackReporter(Integer stepId, Integer patientId) {
        videoWatchedDuration = (double) 0;
        lastTime = (double) 0;
        Service service = new Service();
        services = service.getService();
        this.stepId = stepId;
        this.patientId = patientId;
    }


    public void sendReport() {
        Log.d("Video", "sendReport: " + videoWatchedDuration);
        if (videoWatchedDuration > 10.0){
            Call<ResponseBody> call = services.set_video_watchtime(patientId, stepId, videoWatchedDuration);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    videoWatchedDuration = 0.0;
                    Log.d("Video", "sendReport: sent");
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Log.e("Error", "call: " + t.getMessage());
                }
            });
        }
    }

    @Override
    public void onProgressUpdate(int i, int i1) {
        Double current = i/1000.0;
        Double delta = current-lastTime;
        if (delta > 0) {
            videoWatchedDuration += delta;
        }
        lastTime = current;
    }
}
