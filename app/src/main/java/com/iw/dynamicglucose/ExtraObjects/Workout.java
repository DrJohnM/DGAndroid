package com.iw.dynamicglucose.ExtraObjects;

/**
 * Created by IW on 31/01/2018.
 */

public class Workout {
    private Integer id;
    private Boolean daytoday;
    private String title;
    private String img;
    private String time;
    private String content;

    public Workout(Integer id, Boolean daytoday, String title, String img, String time, String content) {
        this.id = id;
        this.daytoday = daytoday;
        this.title = title;
        this.img = img;
        this.time = time;
        this.content = content;
    }

    public Workout(){

    }

    public Integer getId() {
        return id;
    }

    public Boolean getDaytoday() {
        return daytoday;
    }

    public String getTitle() {
        return title;
    }

    public String getImg() {
        return img;
    }

    public String getTime() {
        return time;
    }

    public String getContent() {
        return content;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setDaytoday(Boolean daytoday) {
        this.daytoday = daytoday;
    }
}
