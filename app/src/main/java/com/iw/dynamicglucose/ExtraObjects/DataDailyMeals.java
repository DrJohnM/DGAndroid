package com.iw.dynamicglucose.ExtraObjects;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by anavictoriafrias on 20/02/18.
 */

public class DataDailyMeals {
    private Integer patient_id;
    private ArrayList<Map<String, Integer>> groups;

    public DataDailyMeals(Integer patient_id, ArrayList<Map<String, Integer>> groups) {
        this.patient_id = patient_id;
        this.groups = groups;
    }

    public DataDailyMeals(){

    }

    public Integer getPatient_id() {
        return patient_id;
    }

    public void setPatient_id(Integer patient_id) {
        this.patient_id = patient_id;
    }

    public ArrayList<Map<String, Integer>> getGroups() {
        return groups;
    }

    public void setGroups(ArrayList<Map<String, Integer>> groups) {
        this.groups = groups;
    }
}
