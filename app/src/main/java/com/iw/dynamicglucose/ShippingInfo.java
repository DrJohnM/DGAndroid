package com.iw.dynamicglucose;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.iw.dynamicglucose.config.Service;
import com.iw.dynamicglucose.config.Session;
import com.iw.dynamicglucose.services.Services;
import com.iw.dynamicglucose.services.applogin.UserSession;
import com.iw.dynamicglucose.services.get_shipping_info.GetShippingInfo;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShippingInfo extends AppCompatActivity {
    private Services services;
    private UserSession user;
    private EditText country;
    private EditText state;
    private EditText city;
    private EditText add1;
    private EditText add2;
    private EditText zip;
    private EditText phone;
    private Integer id;

    //loader
    private MaterialDialog dialogGetInfo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shipping_info);
        getSupportActionBar().hide();
        Service service = new Service();
        services = service.getService();
        Session session = new Session(getBaseContext());
        user = session.getSession();

        country = findViewById(R.id.input_country_shipping);
        state = findViewById(R.id.input_state_shipping);
        city = findViewById(R.id.input_city_shipping);
        add1 = findViewById(R.id.input_address1_shipping);
        add2 = findViewById(R.id.input_address2_shipping);
        zip = findViewById(R.id.input_zip_shipping);
        phone = findViewById(R.id.input_phone_shipping);
        getShipping();
        Button save = findViewById(R.id.btn_save_shipping);
        ImageButton back = findViewById(R.id.btn_back_shipping);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                update();
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
    private void getShipping(){
        dialogGetInfo = loader();
        dialogGetInfo.show();
        // Create the call of the service view program progress
        Call<GetShippingInfo> call = services.get_shipping_info(user.getId().toString());

        // Executing call of the service view program progress
        call.enqueue(new Callback<GetShippingInfo>() {
            @Override
            public void onResponse(Call<GetShippingInfo> call, Response<GetShippingInfo> response) {
                switch (response.code()) {
                    case 200:
                        GetShippingInfo data = response.body();
                        assert data != null;
                        country.setHint(data.getShoppingAddress().getCountry());
                        state.setHint(data.getShoppingAddress().getState());
                        city.setHint(data.getShoppingAddress().getCity());
                        add1.setHint(data.getShoppingAddress().getAddress1());
                        add2.setHint(data.getShoppingAddress().getAddress2());
                        zip.setHint(data.getShoppingAddress().getPostalCode());
                        phone.setHint(data.getShoppingAddress().getPhone());
                        id = data.getShoppingAddress().getId();
                        break;
                    default:

                        break;
                }
                dialogGetInfo.dismiss();
            }

            @Override
            public void onFailure(Call<GetShippingInfo> call, Throwable t) {
                //progressBarGet.setVisibility(View.GONE);
                createToast("Error: " + t.getMessage());
                dialogGetInfo.dismiss();
            }
        });
    }
    private void update(){
        dialogGetInfo = loader();
        dialogGetInfo.show();
        String txtCountry;
        if(country.getText().toString().equals("")){
            txtCountry = country.getHint().toString();
        }else{
            txtCountry = country.getText().toString();
        }
        String txtState;
        if(state.getText().toString().equals("")){
            txtState = state.getHint().toString();
        }else{
            txtState = state.getText().toString();
        }
        String txtCity;
        if(city.getText().toString().equals("")){
            txtCity = city.getHint().toString();
        }else{
            txtCity = city.getText().toString();
        }
        String txtAdd1;
        if(add1.getText().toString().equals("")){
            txtAdd1 = add1.getHint().toString();
        }else{
            txtAdd1 = add1.getText().toString();
        }
        String txtAdd2;
        if(add2.getText().toString().equals("")){
            txtAdd2 = add2.getHint().toString();
        }else{
            txtAdd2 = add2.getText().toString();
        }
        String txtZip;
        if(zip.getText().toString().equals("")){
            txtZip = zip.getHint().toString();
        }else{
            txtZip = zip.getText().toString();
        }
        String txtPhone;
        if(phone.getText().toString()       .equals("")){
            txtPhone = phone.getHint().toString();
        }else{
            txtPhone = phone.getText().toString();
        }

        // Create the call of the service view program progress
        Call<ResponseBody> call = services.update_shipping_info(id.toString(), txtCity, txtAdd1, txtAdd2, txtPhone, txtZip, txtCountry, txtState);

        // Executing call of the service time to start
        //progressBarSet.setVisibility(View.VISIBLE);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                switch (response.code()) {
                    case 200:
                        //progressBarSet.setVisibility(View.GONE);
                        finish();
                        break;
                    default:
                        break;
                }
                dialogGetInfo.dismiss();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                //progressBarSet.setVisibility(View.GONE);
                dialogGetInfo.dismiss();
                createToast("Error" + t);
            }
        });
    }
    private void createToast(String msg){
        Toast toast = Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    private MaterialDialog loader(){
        MaterialDialog.Builder builder = new MaterialDialog.Builder(this)
                .content("Please wait...")
                .progress(true, 0);
        return builder.build();
    }
}
