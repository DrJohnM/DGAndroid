package com.iw.dynamicglucose;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;

public class LifestyleQuestionWarning extends AppCompatActivity {

    private RadioButton rb1_1;
    private RadioButton rb1_2;
    private Button btn_lifestyle;

    private Boolean status1 = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lifestyle_question_warning);
        getSupportActionBar().hide();

        rb1_1= findViewById(R.id.rb1_1);
        rb1_2= findViewById(R.id.rb1_2);
        btn_lifestyle = findViewById(R.id.btn_lifestyle);

        rb1_2.setChecked(true);
        rb1_1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    status1 = true;
                }
            }
        });

        rb1_2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    status1 = false;
                }
            }
        });

        btn_lifestyle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(status1){
                    Intent i = new Intent(getApplicationContext(),Warning1Lifestyle.class);
                    startActivity(i);
                    finish();
                }else{
                    Intent i = new Intent(getApplicationContext(),LifestyleVideo.class);
                    startActivity(i);
                    finish();
                }
            }
        });
    }
}
