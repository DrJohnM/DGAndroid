package com.iw.dynamicglucose;

import android.content.Context;
import android.content.Intent;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.iw.dynamicglucose.services.view_onboarding_articles.Article;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AdapterArticlesMindset extends RecyclerView.Adapter<AdapterArticlesMindset.ViewHolder> {

    private List<Article> articles;
    private Context context;
    private Boolean showReason;

    AdapterArticlesMindset(Context context, List<Article> articles, Boolean showReason) {
        this.articles = articles;
        this.context = context;
        this.showReason = showReason;
    }


    @Override
    public AdapterArticlesMindset.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AdapterArticlesMindset.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_mindset_article, parent, false));
    }

    @Override
    public void onBindViewHolder(AdapterArticlesMindset.ViewHolder holder, int position) {
        final Article article = articles.get(position);
        holder.txt_article.setText(article.getTitle());
        Picasso.with(context).load(article.getImage()).into(holder.img_article);
        holder.article_content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(context, SingleArticle.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.putExtra("article_id", article.getId());
                i.putExtra("showReason", !showReason);
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return articles.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder{

        ImageView img_article;
        TextView txt_article;
        CardView article_content;

        ViewHolder(View itemView) {
            super(itemView);
            txt_article = itemView.findViewById(R.id.txt_article);
            img_article = itemView.findViewById(R.id.img_article);
            article_content = itemView.findViewById(R.id.article_content);
        }
    }
}
