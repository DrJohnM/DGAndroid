package com.iw.dynamicglucose;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class PrevFoodSelection extends AppCompatActivity {

    private Button lets_go;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prev_food_selection);
        getSupportActionBar().hide();

        lets_go = findViewById(R.id.lets_go);
        lets_go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), OnboardingMeats.class);
                startActivity(i);
                finish();
            }
        });
    }
}
