package com.iw.dynamicglucose;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.iw.dynamicglucose.config.Session;
import com.iw.dynamicglucose.services.Members.Members;

import com.iw.dynamicglucose.services.Members.User;
import com.iw.dynamicglucose.services.applogin.UserSession;
import com.squareup.picasso.Picasso;

/**
 * Created by IW on 06/02/2018.
 */

public class AdapterMembers extends BaseAdapter {
    private LayoutInflater inflator;
    private Context context;
    private Members members;
    private UserSession user_me;

    AdapterMembers(Context context, Members members){
        inflator = LayoutInflater.from(context);
        this.members = members;
        this.context = context;
        Session session = new Session(context);
        user_me = session.getSession();
    }
    @Override
    public int getCount() {
        return members.getUsers().size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        final User user = members.getUsers().get(i);
        View members_view;
        if(user.getPatientId().equals(user_me.getId())){
            members_view = inflator.inflate(R.layout.cell_members_me, (ViewGroup) viewGroup.getRootView(), false);
        }else{
            members_view = inflator.inflate(R.layout.cell_members, (ViewGroup) viewGroup.getRootView(), false);
        }
        TextView title = members_view.findViewById(R.id.title_members);
        title.setText(user.getName());
        ImageView img = members_view.findViewById(R.id.img_cell_members);
        if(!user.getAvatar().equals("")){
            Picasso.with(context).load(user.getAvatar()).into(img);
        }else{
            Picasso.with(context).load(R.drawable.user).into(img);
        }
        members_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(context, SingleUser.class);
                i.putExtra("user_id", user.getPatientId());
                context.startActivity(i);
            }
        });
        return members_view;
    }
}
