package com.iw.dynamicglucose;

import android.annotation.SuppressLint;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.iw.dynamicglucose.config.Service;
import com.iw.dynamicglucose.config.Session;
import com.iw.dynamicglucose.services.Services;
import com.iw.dynamicglucose.services.applogin.UserSession;
import com.iw.dynamicglucose.services.single_members.SingleMember;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SingleUser extends AppCompatActivity {

    private Services services;

    // Declare other vars
    private MaterialDialog loader;

    private ImageView img;
    private TextView title;
    private TextView ubication;
    private TextView age;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_user);
        getSupportActionBar().hide();
        Service service = new Service();
        services = service.getService();
        Session session = new Session(getBaseContext());
        UserSession user = session.getSession();
        Intent i = getIntent();
        img = findViewById(R.id.img_single_user);
        title = findViewById(R.id.txt_title_single_user);
        ubication = findViewById(R.id.txt_ubication_single_user);
        age = findViewById(R.id.txt_age_single_user);
        ImageButton back = findViewById(R.id.backButton_single_user);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        loadSingleMember(i.getIntExtra("user_id", user.getId()));
    }

    private void loadSingleMember(Integer id){
        loader = loader();
        loader.show();
        // Create the call of the service view program progress
        Call<SingleMember> call = services.get_single_member(id.toString());

        // Executing call of the service view program progress
        call.enqueue(new Callback<SingleMember>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<SingleMember> call, Response<SingleMember> response) {
                switch (response.code()) {
                    case 200:
                        SingleMember data = response.body();
                        assert data != null;
                        title.setText(data.getName());
                        age.setText(data.getAge());
                        ubication.setText(data.getCity() + ", " + data.getCountry());
                        if (!data.getAvatar().equals("")) {
                            Picasso.with(getApplicationContext()).load(data.getAvatar()).into(img);
                        }else{
                            Picasso.with(getApplicationContext()).load(R.drawable.user).into(img);
                        }
                        break;
                    default:
                        MaterialDialog.Builder dialog = createMessage();
                        dialog.build();
                        dialog.show();
                        break;
                }
                loader.dismiss();
            }

            @Override
            public void onFailure(Call<SingleMember> call, Throwable t) {
                loader.dismiss();
                MaterialDialog.Builder dialog = createMessage();
                dialog.build();
                dialog.show();
            }
        });
    }



    private MaterialDialog.Builder createMessage(){
        return new MaterialDialog.Builder(this)
                .title("Error")
                .content("Server not available, try again later")
                .positiveText("Accept");
    }

    private MaterialDialog loader(){
        MaterialDialog.Builder builder = new MaterialDialog.Builder(this)
                .content("Please wait...")
                .progress(true, 0);
        return builder.build();
    }
}
