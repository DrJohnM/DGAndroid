package com.iw.dynamicglucose;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.iw.dynamicglucose.config.BaseUrl;
import com.iw.dynamicglucose.config.Service;
import com.iw.dynamicglucose.config.Session;
import com.iw.dynamicglucose.services.Services;
import com.iw.dynamicglucose.services.applogin.UserSession;
import com.iw.dynamicglucose.services.time_to_start.TimeToStart;
import com.iw.dynamicglucose.services.view_onboarding_articles.ViewOnboardingArticles;
import com.onesignal.OneSignal;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
public class WatingScreen extends AppCompatActivity {

    // Declare vars layout
    private TextView textDays;
    private GridView gridArticles;
    private Button goal;

    // Declare other vars
    private AdapterGridArticles adapterGridArticles;

    private Services services;
    private UserSession user;

    //loader
    private MaterialDialog dialogWaiting;
    private MaterialDialog dialogArticles;
    private TextView btn_foods;
    private Button btn_knowledge_hub;
    private BaseUrl baseUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_wating_screen);

        // Assignment vars layout
        textDays = findViewById(R.id.lblTextDays);
        gridArticles = findViewById(R.id.grid_articles);
        goal = findViewById(R.id.containerGoal);
        btn_foods = findViewById(R.id.btn_foods);
        btn_knowledge_hub =findViewById(R.id.btn_knowledge_hub);
        // Assignment vars config
        Service service = new Service();
        services = service.getService();
        Session session = new Session(getBaseContext());
        user = session.getSession();
        baseUrl = new BaseUrl();
        OneSignal.setSubscription(true);
        register_device();
        goal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i  = new Intent(getApplicationContext(), Goal.class);
                startActivity(i);

            }
        });
        // Call services for load screen
        //loadScreen();
        //loadArticles();


        btn_foods.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent viewIntent =
                        new Intent("android.intent.action.VIEW",
                                Uri.parse(baseUrl.getBaseurl() + "users/pick_food/" + user.getToken() + "/"));
                startActivity(viewIntent);
            }
        });
        btn_knowledge_hub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent viewIntent =
                        new Intent("android.intent.action.VIEW",
                                Uri.parse(baseUrl.getBaseurl() + "users/get_knowledge_hub/" + user.getToken() + "/"));
                startActivity(viewIntent);
            }
        });
    }

    private void loadScreen(){
        dialogWaiting = loader();
        dialogWaiting.show();
        // Create the call of the service time to start
        Call<TimeToStart> call = services.time_to_start(user.getId());

        // Executing call of the service time to start
        call.enqueue(new Callback<TimeToStart>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<TimeToStart> call, Response<TimeToStart> response) {
                switch (response.code()){
                    case 200:
                        // Getting data objects
                        TimeToStart data = response.body();
                        // Set value to service response to label text days
                        assert data != null;
                        Integer time_start = 8 - response.body().getTimeToStart();
                        textDays.setText(time_start.toString());
                        break;
                    default:
                        MaterialDialog.Builder dialog = createMessage();
                        dialog.build();
                        dialog.show();
                        break;
                }
                dialogWaiting.dismiss();
            }

            @Override
            public void onFailure(Call<TimeToStart> call, Throwable t) {
                dialogWaiting.dismiss();
                MaterialDialog.Builder dialog = createMessage();
                dialog.build();
                dialog.show();
            }
        });

        // Create the call of the service time to start
        Call<ResponseBody> call2 = services.check_available_food_order(user.getId());

        // Executing call of the service time to start
        call2.enqueue(new Callback<ResponseBody>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                switch (response.code()){
                    case 200:
                        btn_foods.setVisibility(View.VISIBLE);
                        break;
                    default:
                        break;
                }
                dialogWaiting.dismiss();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }
    private void register_device(){
        OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
            @Override
            public void idsAvailable(String userId, String registrationId) {
                if (userId != null){
                    Call<ResponseBody> call = services.register_device(user.getId(), userId);
                    call.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                        }

                        @Override
                        public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {

                        }
                    });

                }
            }
        });
    }
    private void loadArticles(){
        dialogArticles = loader();
        dialogArticles.show();
        // Create the call of the service time to start
        Call<ViewOnboardingArticles> call = services.view_onboarding_articles(user.getId());

        // Executing call of the service time to start
        call.enqueue(new Callback<ViewOnboardingArticles>() {
            @Override
            public void onResponse(Call<ViewOnboardingArticles> call, Response<ViewOnboardingArticles> response) {
                switch (response.code()){
                    case 200:
                        // Getting data objects
                        ViewOnboardingArticles data = response.body();
                        assert data != null;
                        adapterGridArticles = new AdapterGridArticles(data.getArticles(), getApplicationContext(), data.getDisableReason());
                        gridArticles.setAdapter(adapterGridArticles);
                        if(data.getShowReason()){
                            if(!data.getDisableReason()){
                                goal.setVisibility(View.VISIBLE);
                            }else{
                                goal.setVisibility(View.INVISIBLE);
                            }
                        }else{
                            goal.setVisibility(View.INVISIBLE);
                        }
                        break;
                    case 204:
                        break;
                    default:
                        MaterialDialog.Builder dialog = createMessage();
                        dialog.build();
                        dialog.show();
                        break;
                }
                dialogArticles.dismiss();
            }

            @Override
            public void onFailure(Call<ViewOnboardingArticles> call, Throwable t) {
                dialogArticles.dismiss();
                MaterialDialog.Builder dialog = createMessage();
                dialog.build();
                dialog.show();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadArticles();
        loadScreen();
    }

    private MaterialDialog.Builder createMessage(){
        return new MaterialDialog.Builder(this)
                .title("Error")
                .content("Server not available, try again later")
                .positiveText("Accept");
    }

    private MaterialDialog loader(){
        MaterialDialog.Builder builder = new MaterialDialog.Builder(this)
                .content("Please wait...")
                .progress(true, 0);
        return builder.build();
    }

}
