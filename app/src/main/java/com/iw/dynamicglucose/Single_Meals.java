package com.iw.dynamicglucose;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.iw.dynamicglucose.ExtraObjects.DataSingleMeal;
import com.iw.dynamicglucose.ExtraObjects.ItemSingleMeals;
import com.iw.dynamicglucose.config.Service;
import com.iw.dynamicglucose.config.Session;
import com.iw.dynamicglucose.services.Services;
import com.iw.dynamicglucose.services.applogin.UserSession;
import com.iw.dynamicglucose.services.single_meals.Item;
import com.iw.dynamicglucose.services.single_meals.SingleMeals;
import com.iw.dynamicglucose.services.view_progress_program.ViewProgramProgress;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Single_Meals extends Fragment {

    private Services services;
    private Session session;
    private UserSession user;
    private RecyclerView list;
    private Button button;
    private RecyclerView.Adapter adapter;
    private SingleMeals meals;
    private TextView txt_description_single_meals;
    private TextView lbl_days_single_meals;
    private BottomNavigationView bottomNavigationView;
    private Boolean come_from_home = false;

    //loader
    private MaterialDialog dialogMeals;

    private Boolean buttonSet;
    private Call doneCall;

    public static Single_Meals newInstance(Bundle arguments){
        Single_Meals f = new Single_Meals();
        if(arguments != null){
            f.setArguments(arguments);
        }
        return f;
    }
    public Single_Meals() {
        // Required empty public constructor
    }

    public static Single_Meals newInstance() {
        return new Single_Meals();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_single__meals, container, false);
    }
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Service service = new Service();
        services = service.getService();
        session = new Session(getContext());
        user = session.getSession();
        txt_description_single_meals = getView().findViewById(R.id.txt_description_single_meals);
        lbl_days_single_meals = getView().findViewById(R.id.lbl_days_single_meals);
        TextView title = getView().findViewById(R.id.txt_single_meals_title);
        list = getView().findViewById(R.id.listView_single_meals);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        list.setLayoutManager(layoutManager);
        button = getView().findViewById(R.id.markdone_single_meals);
        ImageView back_button_meals = getView().findViewById(R.id.back_button_meals);
        bottomNavigationView = ((Home)getActivity()).findViewById(R.id.navigation);
        title.setText(getArguments().getString("meal_time_string"));
        come_from_home = getArguments().getBoolean("come_from_home", false);
        loadMealsList();
        loadProgramProgress();
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                markAsDone();
            }
        });
        back_button_meals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(come_from_home){
                    MenuItem item = bottomNavigationView.getMenu().getItem(0);
                    item.setChecked(true);
                    fragment_home fragment = fragment_home.newInstance();
                    FragmentTransaction ft = ((FragmentActivity)getContext()).getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.frame_layout, fragment);
                    ft.commit();
                }else{
                    MenuItem item = bottomNavigationView.getMenu().getItem(1);
                    item.setChecked(true);
                    fragment_meals fragment = fragment_meals.newInstance();
                    FragmentTransaction ft = ((FragmentActivity)getContext()).getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.frame_layout, fragment);
                    ft.commit();
                }
            }
        });
    }
    private void setButton(boolean done){
        if (done){
            button.setBackground(getResources().getDrawable(R.drawable.yellow_selected_rounded_button));
            button.setTextColor(Color.WHITE);
            button.setText(getString(R.string.done));

        }else {
            button.setBackground(getResources().getDrawable(R.drawable.yellow_rounded_button));
            button.setTextColor(getResources().getColor(R.color.purpleGray));
            button.setText(getString(R.string.markAsDone));
        }
        buttonSet = done;
    }
    private void markAsDone(){
        // Create the call of the service time to start
        ArrayList<ItemSingleMeals> temp = new ArrayList<>();

        for (Item item : meals.getItems()) {
            ItemSingleMeals b = new ItemSingleMeals(item.getId(), item.getType());
            temp.add(b);
        }
        final Boolean previous = buttonSet;
        setButton(!previous);
        DataSingleMeal data = new DataSingleMeal(user.getId(), 1, meals.getMealTime(), temp, !previous);
        Call<ResponseBody> call = services.set_progress_day_food_items(data);
        if(doneCall != null){
            doneCall.cancel();
        }
        doneCall = call;



        // Executing call of the service time to start
        call.enqueue(new Callback<ResponseBody>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() != 200) {
                    setButton(previous);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (!call.isCanceled()){
                    setButton(previous);
                    createToast("Error: " + t.getMessage());
                }
            }
        });
    }
    private void loadProgramProgress(){
        // Create the call of the service view program progress
        Call<ViewProgramProgress> call = services.view_program_progress(user.getId());

        // Executing call of the service view program progress
        call.enqueue(new Callback<ViewProgramProgress>() {
            @Override
            public void onResponse(Call<ViewProgramProgress> call, Response<ViewProgramProgress> response) {
                switch (response.code()){
                    case 200:
                        ViewProgramProgress data = response.body();
                        assert data != null;
                        if(data.getIs_active()){
                            lbl_days_single_meals.setText(data.getProgress_program());
                        }else{
                            session.CloseSession();
                            Intent i = new Intent(getContext(), Login.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            startActivity(i);
                            getActivity().finish();
                        }

                        break;
                    default:
                        MaterialDialog.Builder dialog = createMessage();
                        dialog.build();
                        dialog.show();
                        break;
                }
            }

            @Override
            public void onFailure(Call<ViewProgramProgress> call, Throwable t) {
                MaterialDialog.Builder dialog = createMessage();
                dialog.build();
                dialog.show();
            }
        });
    }
    private void loadMealsList(){
        dialogMeals = loader();
        dialogMeals.show();
        // Create the call of the service view program progress
        Call<SingleMeals> call = services.get_single_meals(user.getId(), getArguments().getString("meal_id"), getArguments().getString("group"));

        // Executing call of the service view program progress
        call.enqueue(new Callback<SingleMeals>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<SingleMeals> call, Response<SingleMeals> response) {
                switch (response.code()){
                    case 200:
                        SingleMeals data = response.body();
                        meals = data;
                        assert data != null;
                        adapter = new AdapterSingleMeals(getContext(), data.getItems());
                        list.setAdapter(adapter);
                        setButton(data.getDone());
                        StringBuilder temp = new StringBuilder();
                        for(Item i: data.getItems()){
                            temp.append(i.getName()).append(": \n").append(i.getDescription()).append("\n\n");
                        }
                        txt_description_single_meals.setText(temp.toString());
                        break;

                    default:
                        createToast("Error code: " + response.code());
                        break;
                }
                dialogMeals.dismiss();
            }

            @Override
            public void onFailure(Call<SingleMeals> call, Throwable t) {
                dialogMeals.dismiss();
            }
        });

    }
    private MaterialDialog.Builder createMessage(){
        return new MaterialDialog.Builder(getContext())
                .title("Error")
                .content("Server not available, try again later")
                .positiveText("Accept");
    }
    private void createToast(String msg){
        Toast toast = Toast.makeText(getContext(), msg, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    private MaterialDialog loader(){
        MaterialDialog.Builder builder = new MaterialDialog.Builder(getContext())
                .content("Please wait...")
                .progress(true, 0);
        return builder.build();
    }
}
