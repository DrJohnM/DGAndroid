package com.iw.dynamicglucose;

import android.app.Application;
import android.content.Intent;
import android.content.res.Configuration;

import com.iw.dynamicglucose.config.Session;
import com.iw.dynamicglucose.config.TypefaceUtil;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;

import org.json.JSONException;

/**
 * Created by IW on 09/02/2018.
 */

public class DynamicGlucose extends Application {
    Session session;
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        session = new Session(getApplicationContext());


        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .setNotificationOpenedHandler(new OneSignal.NotificationOpenedHandler() {
                    public void notificationOpened(OSNotificationOpenResult result) {
                        try {
                            if(result.notification.payload.additionalData.get("code").equals("1")){
                                session.setCodeNotif(1);
                                Intent i = new Intent(getApplicationContext(), SplashScreen.class)
                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                startActivity(i);
                            }else if(result.notification.payload.additionalData.get("code").equals("2")){
                                session.setCodeNotif(2);
                                Intent i = new Intent(getApplicationContext(), SplashScreen.class)
                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                startActivity(i);
                            }else if(result.notification.payload.additionalData.get("code").equals("3")){
                                session.setCodeNotif(3);
                                Intent i = new Intent(getApplicationContext(), SplashScreen.class)
                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                startActivity(i);
                            }else if(result.notification.payload.additionalData.get("code").equals("4")){
                                session.setCodeNotif(4);
                                Intent i = new Intent(getApplicationContext(), SplashScreen.class)
                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                startActivity(i);
                            }else if(result.notification.payload.additionalData.get("code").equals("5")){
                                session.setCodeNotif(5);
                                Intent i = new Intent(getApplicationContext(), SplashScreen.class)
                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                startActivity(i);
                            }
                        } catch (JSONException ignored) {

                        }
                    }

                })
                .init();

        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/SFUIDisplay-Medium.ttf");
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}
