package com.iw.dynamicglucose;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.halilibo.bvpkotlin.*;
import com.iw.dynamicglucose.config.Service;
import com.iw.dynamicglucose.config.Session;
import com.iw.dynamicglucose.services.Services;
import com.iw.dynamicglucose.services.applogin.UserSession;
import com.iw.dynamicglucose.services.view_progress_program.ViewProgramProgress;
import com.iw.dynamicglucose.ExtraObjects.VideoProgressCallbackReporter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class fragment_single_workout extends Fragment implements VideoCallback {

    private TextView txtDays;

    private Services services;
    private Session session;
    private UserSession user;
    private ImageView back_button_meals;
    private Integer workoutID;

    private MaterialDialog dialogDays;
    private BetterVideoPlayer player;

    private VideoProgressCallbackReporter videoProgressCallbackReporter;

    public static fragment_single_workout newInstance(Bundle arguments){
        fragment_single_workout f = new fragment_single_workout();
        if(arguments != null){
            f.setArguments(arguments);
        }
        return f;
    }
    public fragment_single_workout() {
        // Required empty public constructor
    }
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        TextView title = getView().findViewById(R.id.txt_title_single_workout);
        TextView time = getView().findViewById(R.id.txt_time_single_workout);
        back_button_meals = getView().findViewById(R.id.back_button_meals);
        TextView content = getView().findViewById(R.id.txt_content_single_workout);
        txtDays = getView().findViewById(R.id.lbl_days);
        player = getView().findViewById(R.id.player);
        player.setCallback(this);
        player.setAutoPlay(true);

        int stepId = getArguments().getInt("stepId");

        title.setText(getArguments().getString("title"));
        time.setText(getArguments().getString("time"));
        content.setText(getArguments().getString("content"));
        if(!getArguments().getString("media").equals("")){
            player.setSource(Uri.parse(getArguments().getString("media")));
        }
        workoutID = getArguments().getInt("id");
        // Assignment vars config
        Service service = new Service();
        services = service.getService();
        session = new Session(getContext());
        user = session.getSession();

        videoProgressCallbackReporter = new VideoProgressCallbackReporter(stepId, user.getId());
        player.setProgressCallback(videoProgressCallbackReporter);

        if(session.getCodeNotif() != 0){
            session.setCodeNotif(0);
        }

        loadProgramProgress();

        back_button_meals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Fragment selectedFragment = null;
//                selectedFragment = fragment_workouts.newInstance();
//                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
//                transaction.replace(R.id.frame_layout, selectedFragment);
//                transaction.commit();
//
                Bundle arguments = new Bundle();
                arguments.putString("id", workoutID.toString());
                fragment_steps_workout fragment = fragment_steps_workout.newInstance(arguments);
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.frame_layout, fragment);
                ft.commit();

            }
        });
    }

    // TODO: Rename and change types and number of parameters
    public static fragment_single_workout newInstance() {
        return new fragment_single_workout();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fragment_single_workout, container, false);
    }

    @Override
    public void onStop() {
        super.onStop();
        videoProgressCallbackReporter.sendReport();
    }

    private void loadProgramProgress(){
        dialogDays = loader();
        dialogDays.show();
        // Create the call of the service view program progress
        Call<ViewProgramProgress> call = services.view_program_progress(user.getId());

        // Executing call of the service view program progress
        call.enqueue(new Callback<ViewProgramProgress>() {
            @Override
            public void onResponse(Call<ViewProgramProgress> call, Response<ViewProgramProgress> response) {
                switch (response.code()){
                    case 200:
                        ViewProgramProgress data = response.body();
                        assert data != null;
                        if(data.getIs_active()){
                            txtDays.setText(data.getProgress_program());
                        }else{
                            session.CloseSession();
                            Intent i = new Intent(getContext(), Login.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            startActivity(i);
                            getActivity().finish();
                        }

                        break;
                    default:
                        MaterialDialog.Builder dialog = createMessage();
                        dialog.build();
                        dialog.show();
                        break;
                }
                dialogDays.dismiss();
            }

            @Override
            public void onFailure(Call<ViewProgramProgress> call, Throwable t) {
                dialogDays.dismiss();
                MaterialDialog.Builder dialog = createMessage();
                dialog.build();
                dialog.show();
            }
        });
    }

    private MaterialDialog.Builder createMessage(){
        return new MaterialDialog.Builder(getContext())
                .title("Error")
                .content("Server not available, try again later")
                .positiveText("Accept");
    }

    private MaterialDialog loader(){
        MaterialDialog.Builder builder = new MaterialDialog.Builder(getContext())
                .content("Please wait...")
                .progress(true, 0);
        return builder.build();
    }

    @Override
    public void onPause() {
        super.onPause();
        player.pause();
        videoProgressCallbackReporter.sendReport();
    }

    @Override
    public void onStarted(BetterVideoPlayer player) {

    }

    @Override
    public void onPaused(BetterVideoPlayer player) {
        videoProgressCallbackReporter.sendReport();
    }

    @Override
    public void onPreparing(BetterVideoPlayer player) {

    }

    @Override
    public void onPrepared(BetterVideoPlayer player) {

    }

    @Override
    public void onBuffering(int percent) {

    }

    @Override
    public void onError(BetterVideoPlayer player, Exception e) {

    }

    @Override
    public void onCompletion(BetterVideoPlayer player) {
        videoProgressCallbackReporter.sendReport();
    }

    @Override
    public void onToggleControls(BetterVideoPlayer player, boolean isShowing) {

    }
}
