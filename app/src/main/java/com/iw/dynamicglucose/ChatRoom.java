package com.iw.dynamicglucose;

import android.content.Intent;
import android.os.Build;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.iw.dynamicglucose.config.BaseUrl;
import com.iw.dynamicglucose.config.Service;
import com.iw.dynamicglucose.config.Session;
import com.iw.dynamicglucose.services.Services;
import com.iw.dynamicglucose.services.applogin.UserSession;
import com.iw.dynamicglucose.services.get_history_message.GetMessages;
import com.iw.dynamicglucose.services.get_history_message.Message;
import com.iw.dynamicglucose.services.view_progress_program.ViewProgramProgress;

import org.apache.commons.lang3.StringEscapeUtils;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tech.gusavila92.websocketclient.WebSocketClient;

public class ChatRoom extends AppCompatActivity {

    private Services services;
    private Session session;
    private UserSession user;
    private BaseUrl baseUrl;

    //Declare list
    private RecyclerView listView;
    private RecyclerView.Adapter adapterChatMessages;
    private EditText editText;
    private ImageButton sendMessageBtn;
    private ImageButton back;
    private ImageButton myGroup;
    private TextView txtDays;

    private MaterialDialog dialog;
    private MaterialDialog dialogDays;
    private String user_name = "";
    private List<Message> messages;

    private WebSocketClient webSocket;
    private com.iw.dynamicglucose.services.Members.Members members;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_room);
        getSupportActionBar().hide();

        //Layout vars
        listView = findViewById(R.id.list_view_chat);
        editText = findViewById(R.id.writeMessage);
        sendMessageBtn = findViewById(R.id.sendButton);
        back = findViewById(R.id.back_chat);
        myGroup = findViewById(R.id.my_group_chat);
        txtDays = findViewById(R.id.lbl_days);

        // Assignment vars config
        Service service = new Service();
        services = service.getService();
        session = new Session(getApplicationContext());
        user = session.getSession();
        baseUrl = new BaseUrl();
        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO){
                    send();
                    return true;
                }
                return false;
            }
        });

        if(session.getCodeNotif() != 0){
            session.setCodeNotif(0);
        }

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        listView.setLayoutManager(layoutManager);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        loadMessages();
        connectWebSocket();
        loadProgramProgress();

        setListeners();

    }

    private void setListeners() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        myGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ChatRoom.this, Members.class);
                startActivity(i);
            }
        });
    }

    private void loadMessages(){
        dialog = loader();
        dialog.show();
        // Create the call of the service to get historical messages
        Call<GetMessages> call = services.get_history_message(user.getDg_program());
        Integer program = user.getDg_program();
        Log.d("TAG", "loadMessages: " + program);
        // Executing call of the service time to start
        call.enqueue(new Callback<GetMessages>() {
            @Override
            public void onResponse(Call<GetMessages> call, Response<GetMessages> response) {
                switch (response.code()) {
                    case 200:
                        GetMessages data = response.body();
                        assert data != null;
                        messages = data.getMessages();
                        for(Message message: data.getMessages()){
                            if(user.getEmail().equals(message.getEmail())){
                                user_name = message.getUser();
                            }
                        }
                        // Create the call of the service view program progress
                        Call<com.iw.dynamicglucose.services.Members.Members> call_m = services.get_members(user.getId().toString());

                        // Executing call of the service view program progress
                        call_m.enqueue(new Callback<com.iw.dynamicglucose.services.Members.Members>() {
                            @Override
                            public void onResponse(Call<com.iw.dynamicglucose.services.Members.Members> call, Response<com.iw.dynamicglucose.services.Members.Members> response) {
                                switch (response.code()){
                                    case 200:
                                        com.iw.dynamicglucose.services.Members.Members data = response.body();
                                        assert data != null;
                                        members = data;
                                        adapterChatMessages = new AdapterChatMessages(getApplicationContext(), messages, data.getUsers());
                                        listView.setAdapter(adapterChatMessages);
                                        listView.scrollToPosition(messages.size() - 1);
                                        break;
                                    default:

                                        break;
                                }
                            }

                            @Override
                            public void onFailure(Call<com.iw.dynamicglucose.services.Members.Members> call, Throwable t) {
                                createToast("Error: " + t.getMessage());
                            }

                        });
                        break;

                    default:
                        createToast("Error" + response.code());
                        break;
                }
                dialog.dismiss();
            }

            @Override
            public void onFailure(Call<GetMessages> call, Throwable t) {
                dialog.dismiss();
                createToast("Error" + t);
            }
        });

    }

    private void connectWebSocket() {
        String url = baseUrl.getUrlSocket() + user.getDg_program() + "/";
        URI uri;
        try{
          uri = new URI(url);
        }catch (URISyntaxException e){
            Log.e("WebSocket", "Create uri: " + e.getMessage());
            return;
        }

        webSocket = new WebSocketClient(uri) {
            @Override
            public void onOpen() {
                Log.e("WebSocket", "Opened");
//                webSocket.send("Connected");
            }

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onTextReceived(String message) {
                String[] complete_msg = message.split("\"");
                Message newMessage = new Message();
                newMessage.setMessage(StringEscapeUtils.unescapeJava(complete_msg[7]));
                newMessage.setUser(complete_msg[11]);
                if(complete_msg[11].equals(user_name)){
                    newMessage.setEmail(session.getSession().getEmail());
                }else{
                    newMessage.setEmail("");
                }
                String[] time_date = complete_msg[3].split(" ");
                newMessage.setTime(time_date[1].substring(0, 5));
                messages.add(newMessage);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        listView.getAdapter().notifyDataSetChanged();
                        listView.scrollToPosition(messages.size() - 1);
                    }
                });

                session.setMessageId(Integer.parseInt(complete_msg[15]));
            }

            @Override
            public void onBinaryReceived(byte[] data) {
                Log.e("WebSocket", "binary");
            }

            @Override
            public void onPingReceived(byte[] data) {
                Log.e("WebSocket", "ping: " + Arrays.toString(data));
            }

            @Override
            public void onPongReceived(byte[] data) {
                Log.e("WebSocket", "pong");
            }

            @Override
            public void onCloseReceived() {
                Log.e("WebSocket", "Closed");
            }

            @Override
            public void onException(Exception ex) {
                Log.e("WebSocket", "Error: " + ex.getMessage());
            }
        };
        webSocket.setConnectTimeout(10000);
        webSocket.setReadTimeout(60000);
        webSocket.enableAutomaticReconnection(5000);
        webSocket.connect();

        sendMessageBtn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View view) {
                // Send message
                send();
            }
        });
    }

    private void send() {
        String toServer = StringEscapeUtils.escapeJava(editText.getText().toString());
        webSocket.send("{\"handle\": \"" + user.getEmail() + "\", \"message\": \"" + toServer + "\"}");
        editText.setText("");
    }

    private void loadProgramProgress(){
        dialogDays = loader();
        dialogDays.show();
        // Create the call of the service view program progress
        Call<ViewProgramProgress> call = services.view_program_progress(user.getId());

        // Executing call of the service view program progress
        call.enqueue(new Callback<ViewProgramProgress>() {
            @Override
            public void onResponse(Call<ViewProgramProgress> call, Response<ViewProgramProgress> response) {
                switch (response.code()){
                    case 200:
                        ViewProgramProgress data = response.body();
                        assert data != null;
                        if(data.getIs_active()){
                            txtDays.setText(data.getProgress_program());
                        }else{
                            session.CloseSession();
                            Intent i = new Intent(getApplicationContext(), Login.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            startActivity(i);
                            finish();
                        }

                        break;
                    default:
                        MaterialDialog.Builder dialog = createMessage();
                        dialog.build();
                        dialog.show();
                        break;
                }
                dialogDays.dismiss();
            }

            @Override
            public void onFailure(Call<ViewProgramProgress> call, Throwable t) {
                dialogDays.dismiss();
                MaterialDialog.Builder dialog = createMessage();
                dialog.build();
                dialog.show();
            }
        });
    }

    private void createToast(String msg){
        Toast toast = Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    private MaterialDialog loader(){
        MaterialDialog.Builder builder = new MaterialDialog.Builder(this)
                .content("Please wait...")
                .progress(true, 0);
        return builder.build();
    }

    private MaterialDialog.Builder createMessage(){
        return new MaterialDialog.Builder(this)
                .title("Error")
                .content("Server not available, try again later")
                .positiveText("Accept");
    }
}
