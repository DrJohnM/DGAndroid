package com.iw.dynamicglucose;


import android.content.Intent;
import android.content.pm.ActivityInfo;
import androidx.annotation.NonNull;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import com.iw.dynamicglucose.config.Service;
import com.iw.dynamicglucose.config.Session;
import com.iw.dynamicglucose.services.Services;
import com.iw.dynamicglucose.services.applogin.UserSession;
import com.onesignal.OneSignal;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Home extends AppCompatActivity {
    Session session;
    private Services services;
    private UserSession user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        session = new Session(getApplicationContext());
        Service service = new Service();
        services = service.getService();
        user = session.getSession();
        setContentView(R.layout.activity_home);
        getSupportActionBar().hide();
        BottomNavigationView bottomNavigationView = findViewById(R.id.navigation);


        //Function for changing views using bottom view
        bottomNavigationView.setOnNavigationItemSelectedListener
                (new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        Fragment selectedFragment = null;
                        switch (item.getItemId()) {
                            case R.id.menu_home:
                                selectedFragment = fragment_home.newInstance();
                                break;
                            case R.id.menu_meals:
                                selectedFragment = fragment_meals.newInstance();
                                break;
                            case R.id.menu_workouts:
                                selectedFragment = fragment_workouts.newInstance();
                                break;
                            case R.id.menu_mindset:
                                selectedFragment = fragment_mindset.newInstance();
                                break;
                        }
                        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.frame_layout, selectedFragment);
                        transaction.commit();
                        return true;
                    }
                });
        if(session.getCodeNotif() == 0){
            //Manually displaying the first fragment - one time only
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frame_layout, fragment_home.newInstance());
            transaction.commit();
        }else if (session.getCodeNotif() == 1){
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frame_layout, fragment_progress.newInstance());
            transaction.commit();
        }else if (session.getCodeNotif() == 2){
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frame_layout, fragment_progress.newInstance());
            transaction.commit();
        }else if (session.getCodeNotif() == 3){
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frame_layout, fragment_meals.newInstance());
            transaction.commit();
        }else if (session.getCodeNotif() == 4){
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frame_layout, fragment_workouts.newInstance());
            transaction.commit();
        }else if (session.getCodeNotif() == 5){
            Intent i = new Intent(getApplicationContext(), ChatRoom.class);
            startActivity(i);
        }
        register_device();
        //Used to select an item programmatically
    }

    private void register_device(){
        OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
            @Override
            public void idsAvailable(String userId, String registrationId) {
                if (userId != null){
                    Call<ResponseBody> call = services.register_device(user.getId(), userId);
                    call.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                        }

                        @Override
                        public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {

                        }
                    });

                }
            }
        });
    }
}
