package com.iw.dynamicglucose;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.iw.dynamicglucose.ExtraObjects.Workout;
import com.iw.dynamicglucose.config.BaseUrl;
import com.iw.dynamicglucose.config.Service;
import com.iw.dynamicglucose.config.Session;
import com.iw.dynamicglucose.services.Services;
import com.iw.dynamicglucose.services.applogin.UserSession;
import com.iw.dynamicglucose.services.view_progress_program.ViewProgramProgress;
import com.iw.dynamicglucose.services.workout_home.TodayWorkouts;
import com.iw.dynamicglucose.services.workout_home.WorkoutFir;
import com.iw.dynamicglucose.services.workout_home.WorkoutSec;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class fragment_workouts extends Fragment {

    private ListView listaWorkouts;
    private ArrayList<Workout> workouts = new ArrayList<Workout>();
    private RelativeLayout showWorkouts;
    private RelativeLayout hiddenWorkouts;
    private TextView txtDays;

    // Declare vars config
    private BaseUrl baseUrl;
    private Services services;
    private UserSession user;
    private Session session;

    // Declare var loader
    private MaterialDialog loader;
    private MaterialDialog dialogDays;
    
    private ArrayList<Call<? extends  Object>> calls = new ArrayList<>();

    public fragment_workouts() {
    }

    // TODO: Rename parameter arguments, choose names that match
    public static fragment_workouts newInstance() {
        return new fragment_workouts();

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        listaWorkouts = getView().findViewById(R.id.list_workouts);
        showWorkouts = getView().findViewById(R.id.show_workouts);
        hiddenWorkouts = getView().findViewById(R.id.hidden_workouts);
        txtDays = getView().findViewById(R.id.lbl_days);

        // Assignment vars config
        Service service = new Service();
        services = service.getService();
        session = new Session(getContext());
        user = session.getSession();

        if(session.getCodeNotif() != 0){
            session.setCodeNotif(0);
        }
        user = session.getSession();
        loadWorkouts();
        loadProgramProgress();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_fragment_workouts, container, false);
    }

    private void loadWorkouts(){
        loader = loader();
        loader.show();

        // Create the call of the service time to start
        Call<TodayWorkouts> call = services.today_workouts(user.getId());

        // Executing call of the service time to start
        call.enqueue(new Callback<TodayWorkouts>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<TodayWorkouts> call, Response<TodayWorkouts> response) {
                switch (response.code()){
                    case 200:
                        // Getting data objects
                        TodayWorkouts data = response.body();
                        // Set value to service response to label text days
                        assert data != null;
                        for(WorkoutFir workoutFir: data.getWorkoutFir()){
                            Workout wor = new Workout();
                            wor.setId(workoutFir.getId());
                            wor.setDaytoday(workoutFir.getDayToDay());
                            wor.setContent(workoutFir.getTitle());
                            wor.setTime(workoutFir.getTime());
                            wor.setImg(workoutFir.getImage());
                            workouts.add(wor);
                        }
                        for(WorkoutSec workoutSec: data.getWorkoutSec()){
                            Workout wor = new Workout();
                            wor.setId(workoutSec.getId());
                            wor.setDaytoday(workoutSec.getDayToDay());
                            wor.setContent(workoutSec.getTitle());
                            wor.setTime(workoutSec.getTime());
                            wor.setImg(workoutSec.getImage());
                            workouts.add(wor);
                        }
                        listaWorkouts.setAdapter(new AdapterWorkoutHome(getContext(), workouts));
                        break;

                    case 204:
                        showWorkouts.setVisibility(View.GONE);
                        hiddenWorkouts.setVisibility(View.VISIBLE);
                        break;

                    default:
                        MaterialDialog.Builder dialog = createMessage();
                        dialog.build();
                        dialog.show();
                        break;
                }
                loader.dismiss();
                calls.remove(call);
            }

            @Override
            public void onFailure(Call<TodayWorkouts> call, Throwable t) {
                loader.dismiss();
                MaterialDialog.Builder dialog = createMessage();
                if (dialog != null){
                    dialog.build();
                    dialog.show();
                }
                calls.remove(call);

            }

        });
        calls.add(call);


    }

    private MaterialDialog.Builder createMessage(){
        Context context = getContext();
        if (context == null){
            return null;
        }
        MaterialDialog.Builder builder = new MaterialDialog.Builder(context)
                .title("Error")
                .content("Server not available, try again later")
                .positiveText("Accept");
        return builder;
    }

    private MaterialDialog loader(){
        Context context = getContext();
        if (context == null){
            return null;
        }
        MaterialDialog.Builder builder = new MaterialDialog.Builder(context)
                .content("Please wait...")
                .progress(true, 0);
        return builder.build();
    }

    private void loadProgramProgress(){
        dialogDays = loader();
        dialogDays.show();
        // Create the call of the service view program progress
        Call<ViewProgramProgress> call = services.view_program_progress(user.getId());

        // Executing call of the service view program progress
        call.enqueue(new Callback<ViewProgramProgress>() {
            @Override
            public void onResponse(Call<ViewProgramProgress> call, Response<ViewProgramProgress> response) {
                switch (response.code()){
                    case 200:
                        ViewProgramProgress data = response.body();
                        assert data != null;
                        if(data.getIs_active()){
                            txtDays.setText(data.getProgress_program());
                        }else{
                            session.CloseSession();
                            Intent i = new Intent(getContext(), Login.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            startActivity(i);
                            getActivity().finish();
                        }

                        break;
                    default:
                        MaterialDialog.Builder dialog = createMessage();
                        dialog.build();
                        dialog.show();
                        break;
                }
                dialogDays.dismiss();
                calls.remove(call);
            }

            @Override
            public void onFailure(Call<ViewProgramProgress> call, Throwable t) {
                if (dialogDays != null){
                    dialogDays.dismiss();
                }
                MaterialDialog.Builder dialog = createMessage();
                if (dialog != null){
                    dialog.build();
                    dialog.show();
                }
                calls.remove(call);

            }
        });
        calls.add(call);
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        //cancel requests that are still executing
        for (Call call : calls) {
            call.cancel();
        }
    }

}
