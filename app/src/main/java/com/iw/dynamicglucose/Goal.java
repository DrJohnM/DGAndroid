package com.iw.dynamicglucose;

import android.content.Intent;
import android.net.Uri;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.widget.VideoView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.halilibo.bvpkotlin.*;
import com.iw.dynamicglucose.config.Service;
import com.iw.dynamicglucose.config.Session;
import com.iw.dynamicglucose.services.Services;
import com.iw.dynamicglucose.services.applogin.UserSession;
import com.iw.dynamicglucose.services.video_goal.VideoGoal;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Goal extends AppCompatActivity implements VideoCallback {
    //layout vars
    private VideoView videoView;
    private BetterVideoPlayer player;
    private EditText editText;
    private ProgressBar videoProgress;

    //loaders
    private MaterialDialog dialogGetVideo;

    private Services services;
    private UserSession user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_goal);
        getSupportActionBar().hide();
        //videoView = findViewById(R.id.video_goal);
        editText = findViewById(R.id.input_goal);
        Button button = findViewById(R.id.btn_goal);
        Service service = new Service();
        services = service.getService();
        Session session = new Session(getBaseContext());
        user = session.getSession();
        videoProgress = findViewById(R.id.progressVideoGoal);
        player = findViewById(R.id.player);
        player.setCallback(this);
        player.setAutoPlay(true);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(editText.getText().toString().equals("")){
                    createToast("Please enter your real reason");
                }else{
                    setGoal(editText.getText().toString());
                }

            }
        });
        getVideo();
    }

    private void getVideo(){
        dialogGetVideo = loader();
        dialogGetVideo.show();
        // Create the call of the service view program progress
        Call<VideoGoal> call = services.get_video_goal();

        // Executing call of the service view program progress
        call.enqueue(new Callback<VideoGoal>() {
            @Override
            public void onResponse(Call<VideoGoal> call, Response<VideoGoal> response) {
                switch (response.code()) {
                    case 200:
                        VideoGoal data = response.body();
                        videoProgress.setVisibility(View.GONE);
                        /*MediaController mediaController = new MediaController(getApplicationContext());
                        mediaController.setAnchorView(videoView);
                        assert data != null;
                        videoView.setVideoURI(Uri.parse(data.getUrl()));
                        videoView.start();*/
                        player.setSource(Uri.parse(data.getUrl()));
                        break;
                    default:

                        break;
                }
                dialogGetVideo.dismiss();
            }

            @Override
            public void onFailure(Call<VideoGoal> call, Throwable t) {
                dialogGetVideo.dismiss();
                createToast("Error: " + t.getMessage());
            }
        });
    }
    private void  setGoal(String msg){
        dialogGetVideo = loader();
        dialogGetVideo.show();
        // Create the call of the service view program progress
        Call<ResponseBody> call = services.set_goal(user.getId(), msg);

        // Executing call of the service time to start
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                switch (response.code()) {
                    case 200:
                        createToast("You have set your real reason successfully");
                        Intent i = new Intent(getApplicationContext(), SplashScreen.class);
                        startActivity(i);
                        finish();
                        break;
                    default:
                        break;
                }
                dialogGetVideo.dismiss();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                //setGoalProgress.setVisibility(View.GONE);
                createToast("Error" + t);
                dialogGetVideo.dismiss();
            }
        });
    }

    private void createToast(String msg){
        Toast toast = Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    private MaterialDialog loader(){
        MaterialDialog.Builder builder = new MaterialDialog.Builder(this)
                .content("Please wait...")
                .progress(true, 0);
        return builder.build();
    }

    @Override
    protected void onPause() {
        super.onPause();
        player.pause();
    }

    @Override
    public void onStarted(BetterVideoPlayer player) {

    }

    @Override
    public void onPaused(BetterVideoPlayer player) {

    }

    @Override
    public void onPreparing(BetterVideoPlayer player) {

    }

    @Override
    public void onPrepared(BetterVideoPlayer player) {

    }

    @Override
    public void onBuffering(int percent) {

    }

    @Override
    public void onError(BetterVideoPlayer player, Exception e) {

    }

    @Override
    public void onCompletion(BetterVideoPlayer player) {

    }

    @Override
    public void onToggleControls(BetterVideoPlayer player, boolean isShowing) {

    }
}
