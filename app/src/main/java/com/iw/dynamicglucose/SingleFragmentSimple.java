package com.iw.dynamicglucose;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.halilibo.bvpkotlin.*;
import com.iw.dynamicglucose.config.BaseUrl;
import com.iw.dynamicglucose.config.Service;
import com.iw.dynamicglucose.config.Session;
import com.iw.dynamicglucose.services.Services;
import com.iw.dynamicglucose.services.applogin.UserSession;
import com.iw.dynamicglucose.services.today_process_mindset.TodayProcessMindset;
import com.iw.dynamicglucose.services.view_progress_program.ViewProgramProgress;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SingleFragmentSimple extends Fragment implements VideoCallback {
    // Declare vars config
    private BaseUrl baseUrl;
    private Services services;
    private Session session;
    private UserSession user;

    private TextView title;
    private TextView time;
    private ImageButton rate1;
    private ImageButton rate2;
    private ImageButton rate3;
    private ImageButton rate4;
    private ImageButton rate5;
    private ImageView back_button_meals;
    private TextView lblrate;
    private TextView content;
    private RelativeLayout showMindset;
    private RelativeLayout hiddenMindset;
    private TextView txtDays;
    private Integer mindset_id;
    // Declare var loader
    private MaterialDialog loader;
    private  MaterialDialog dialogDays;
    private BetterVideoPlayer player;
    private int _rating = 0;


    public SingleFragmentSimple() {
        // Required empty public constructor
    }

    public static SingleFragmentSimple newInstance(Bundle arguments) {
        SingleFragmentSimple fragment = new SingleFragmentSimple();
        if(arguments != null){
            fragment.setArguments(arguments);
        }
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_single_fragment_simple, container, false);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //Llamada de variables
        title = getView().findViewById(R.id.txt_title_mindset);
        time = getView().findViewById(R.id.txt_time_mindset);
        rate1 = getView().findViewById(R.id.btn_rate_1_mindset);
        rate2 = getView().findViewById(R.id.btn_rate_2_mindset);
        rate3 = getView().findViewById(R.id.btn_rate_3_mindset);
        rate4 = getView().findViewById(R.id.btn_rate_4_mindset);
        rate5 = getView().findViewById(R.id.btn_rate_5_mindset);
        lblrate = getView().findViewById(R.id.txt_rate_mindset);
        content = getView().findViewById(R.id.txt_content_mindset);
        showMindset = getView().findViewById(R.id.show_mindset);
        hiddenMindset = getView().findViewById(R.id.hidden_mindset);
        back_button_meals = getView().findViewById(R.id.back_button_meals);
        txtDays = getView().findViewById(R.id.lbl_days);
        player = getView().findViewById(R.id.player);
        player.setCallback(SingleFragmentSimple.this);
        player.setAutoPlay(true);

        mindset_id = getArguments().getInt("mindset_id");

        // Assignment vars config
        Service service = new Service();
        services = service.getService();
        session = new Session(getContext());
        user = session.getSession();

        rate1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rate(user.getId(), mindset_id, 1);
            }
        });
        rate2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rate(user.getId(), mindset_id, 2);
            }
        });
        rate3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rate(user.getId(), mindset_id, 3);
            }
        });
        rate4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rate(user.getId(), mindset_id, 4);
            }
        });
        rate5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rate(user.getId(), mindset_id, 5);
            }
        });

        loadProcessMindset(mindset_id);
        loadProgramProgress();

        back_button_meals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_layout, MindsetList.newInstance());
                transaction.commit();
            }
        });
    }
    private void setRate(Integer rating) {
        _rating = rating;
        ImageButton[] rates = {rate1, rate2, rate3, rate4, rate5};
        for (ImageButton rate: rates) {
            rate.setImageResource(R.drawable.star);
        }
        for (int i=0; i<_rating; i++){
            rates[i].setImageResource(R.drawable.star_checked);
        }
    }

    private void rate(Integer patient_id, Integer mindset_id, final Integer rating){
        final int oldRate = _rating;
        setRate(rating);

        // Create the call of the service time to start
        Call<ResponseBody> call = services.set_rating_mindset(patient_id,mindset_id,rating);

        // Executing call of the service time to start
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() != 200) {
                    setRate(oldRate);
                    MaterialDialog.Builder dialog = createMessage();
                    dialog.build();
                    dialog.show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                MaterialDialog.Builder dialog = createMessage();
                dialog.build();
                dialog.show();
                setRate(oldRate);
            }
        });
    }

    private void loadProcessMindset(Integer id_mindset){
        // Create the call of the service time to start
        Call<TodayProcessMindset> call = services.today_process_mindset(id_mindset, user.getId());

        // Executing call of the service time to start
        call.enqueue(new Callback<TodayProcessMindset>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<TodayProcessMindset> call, Response<TodayProcessMindset> response) {
                switch (response.code()){
                    case 200:
                        // Getting data objects
                        TodayProcessMindset data = response.body();
                        assert data != null;
                        player.setSource(Uri.parse(data.getMedia()));
                        title.setText(data.getTitle());
                        time.setText(data.getDuration());
                        content.setText(data.getDescription());
                        lblrate.setText(data.getRating().toString() + "/5");
                        switch (data.getRating()){
                            case 0:{
                                break;
                            }
                            case 1:{
                                rate1.setImageResource(R.drawable.star_checked);
                                break;
                            }
                            case 2:{
                                rate1.setImageResource(R.drawable.star_checked);
                                rate2.setImageResource(R.drawable.star_checked);
                                break;
                            }
                            case 3:{
                                rate1.setImageResource(R.drawable.star_checked);
                                rate2.setImageResource(R.drawable.star_checked);
                                rate3.setImageResource(R.drawable.star_checked);
                                break;
                            }
                            case 4:{
                                rate1.setImageResource(R.drawable.star_checked);
                                rate2.setImageResource(R.drawable.star_checked);
                                rate3.setImageResource(R.drawable.star_checked);
                                rate4.setImageResource(R.drawable.star_checked);
                                break;
                            }
                            case 5:{
                                rate1.setImageResource(R.drawable.star_checked);
                                rate2.setImageResource(R.drawable.star_checked);
                                rate3.setImageResource(R.drawable.star_checked);
                                rate4.setImageResource(R.drawable.star_checked);
                                rate5.setImageResource(R.drawable.star_checked);
                                break;
                            }
                            default:{
                                break;
                            }
                        }
                        break;

                    case 204:
                        showMindset.setVisibility(View.GONE);
                        hiddenMindset.setVisibility(View.VISIBLE);
                        break;

                    default:
                        MaterialDialog.Builder dialog = createMessage();
                        dialog.build();
                        dialog.show();
                        break;
                }
            }

            @Override
            public void onFailure(Call<TodayProcessMindset> call, Throwable t) {
                Log.e("Error", "call: " + t.getMessage());
                MaterialDialog.Builder dialog = createMessage();
                dialog.build();
                dialog.show();
            }


        });
    }

    private void loadProgramProgress(){
        dialogDays = loader();
        dialogDays.show();
        // Create the call of the service view program progress
        Call<ViewProgramProgress> call = services.view_program_progress(user.getId());

        // Executing call of the service view program progress
        call.enqueue(new Callback<ViewProgramProgress>() {
            @Override
            public void onResponse(Call<ViewProgramProgress> call, Response<ViewProgramProgress> response) {
                switch (response.code()){
                    case 200:
                        ViewProgramProgress data = response.body();
                        assert data != null;
                        if(data.getIs_active()){
                            txtDays.setText(data.getProgress_program());
                        }else{
                            session.CloseSession();
                            Intent i = new Intent(getContext(), Login.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            startActivity(i);
                            getActivity().finish();
                        }

                        break;
                    default:
                        MaterialDialog.Builder dialog = createMessage();
                        dialog.build();
                        dialog.show();
                        break;
                }
                dialogDays.dismiss();
            }

            @Override
            public void onFailure(Call<ViewProgramProgress> call, Throwable t) {
                dialogDays.dismiss();
                MaterialDialog.Builder dialog = createMessage();
                dialog.build();
                dialog.show();
            }
        });
    }

    private MaterialDialog.Builder createMessage(){
        return new MaterialDialog.Builder(getContext())
                .title("Error")
                .content("Server not available, try again later")
                .positiveText("Accept");
    }

    private MaterialDialog loader(){
        MaterialDialog.Builder builder = new MaterialDialog.Builder(getContext())
                .content("Please wait...")
                .progress(true, 0);
        return builder.build();
    }

    @Override
    public void onPause() {
        super.onPause();
        player.pause();
    }

    @Override
    public void onStarted(BetterVideoPlayer player) {

    }

    @Override
    public void onPaused(BetterVideoPlayer player) {

    }

    @Override
    public void onPreparing(BetterVideoPlayer player) {

    }

    @Override
    public void onPrepared(BetterVideoPlayer player) {
        player.start();
    }

    @Override
    public void onBuffering(int percent) {

    }

    @Override
    public void onError(BetterVideoPlayer player, Exception e) {

    }

    @Override
    public void onCompletion(BetterVideoPlayer player) {

    }

    @Override
    public void onToggleControls(BetterVideoPlayer player, boolean isShowing) {

    }
}
