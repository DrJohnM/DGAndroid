package com.iw.dynamicglucose;

import androidx.core.app.NotificationManagerCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Switch;

import com.iw.dynamicglucose.config.Session;
import com.onesignal.OneSignal;
//import com.pushbots.push.Pushbots;

public class SettingsNotifications extends AppCompatActivity {

    private Switch aswitch;
    private Session session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_notifications);
        getSupportActionBar().hide();


        aswitch = findViewById(R.id.switch_notifications);
        ImageButton back = findViewById(R.id.btn_back_notifications);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        session = new Session(getApplicationContext());

        if(session.getstateNotif()){
            aswitch.setChecked(true);
        }else{
            aswitch.setChecked(false);
        }

        aswitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(aswitch.isChecked()){
                    OneSignal

                            .setSubscription(true);
                    session.setstateNotif(true);

                }else{
                    OneSignal.setSubscription(false);
                    session.setstateNotif(false);
                }
            }
        });
    }
}
