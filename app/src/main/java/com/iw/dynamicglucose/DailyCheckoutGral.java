package com.iw.dynamicglucose;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.iw.dynamicglucose.config.Service;
import com.iw.dynamicglucose.config.Session;
import com.iw.dynamicglucose.services.Services;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DailyCheckoutGral extends AppCompatActivity {

    private RadioButton rb1_1;
    private RadioButton rb1_2;
    private RadioButton rb2_1;
    private RadioButton rb2_2;
    private RadioButton rb3_1;
    private RadioButton rb3_2;
    private RadioButton rb4_1;
    private RadioButton rb4_2;
    private ImageButton backButtonDailyWorkouts;
    private ImageButton closeButtonDailyWorkouts;
    private Button btm_daily_workouts;
    private Integer status1 = -1;
    private Integer status2 = -1;
    private Integer status3 = -1;
    private Integer status4 = -1;
    private MaterialDialog dialogLifestyle;
    private ImageButton close;

    private Service service;
    private Services services;
    private Session session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_checkout_gral);
        getSupportActionBar().hide();

        rb1_1= findViewById(R.id.rb1_1);
        rb1_2= findViewById(R.id.rb1_2);
        rb2_1= findViewById(R.id.rb2_1);
        rb2_2= findViewById(R.id.rb2_2);
        rb3_1= findViewById(R.id.rb3_1);
        rb3_2= findViewById(R.id.rb3_2);
        rb4_1= findViewById(R.id.rb4_1);
        rb4_2= findViewById(R.id.rb4_2);
        backButtonDailyWorkouts = findViewById(R.id.backButtonDailyWorkouts);
        closeButtonDailyWorkouts = findViewById(R.id.closeButtonDailyWorkouts);
        btm_daily_workouts = findViewById(R.id.btm_daily_workouts);
        close = findViewById(R.id.closeButtonDailyWorkouts);

        session = new Session(getApplicationContext());
        service = new Service();
        services = service.getService();

        setOnClicks();
    }

    public void setOnClicks(){
        rb1_1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    status1 = 1;
                }
            }
        });

        rb1_2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    status1 = 0;
                }
            }
        });

        rb2_1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    status2 = 1;
                }
            }
        });

        rb2_2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    status2 = 0;
                }
            }
        });

        rb3_1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    status3 = 1;
                }
            }
        });

        rb3_2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    status3 = 0;
                }
            }
        });

        rb4_1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    status4 = 1;
                }
            }
        });

        rb4_2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    status4 = 0;
                }
            }
        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), Home.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(i);
                finish();
            }
        });
        backButtonDailyWorkouts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btm_daily_workouts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (status1 == -1 || status2 == -1 || status3 == -1 || status4 == -1) {
                    Toast toast = Toast.makeText(getApplicationContext(), "Please answer all questions", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    return;
                }

                dialogLifestyle = loader();
                dialogLifestyle.show();
                Call<ResponseBody> call = services.register_final_information(status1, status2, status3, status4, session.getSession().getId());
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        switch (response.code()) {
                            case 200:
                                Intent i = new Intent(getApplicationContext(), FinishCheck.class);
                                startActivity(i);
                                break;
                            default:
                                break;
                        }
                        dialogLifestyle.dismiss();
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        dialogLifestyle.dismiss();
                    }
                });
            }
        });
    }

    private MaterialDialog loader(){
        MaterialDialog.Builder builder = new MaterialDialog.Builder(this)
                .content("Please wait...")
                .progress(true, 0);
        return builder.build();
    }
}
