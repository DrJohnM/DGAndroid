package com.iw.dynamicglucose;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.*;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.content.FileProvider;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.iw.dynamicglucose.ExtraObjects.DataEditProfile;
import com.iw.dynamicglucose.config.Service;
import com.iw.dynamicglucose.config.Session;
import com.iw.dynamicglucose.services.Services;
import com.iw.dynamicglucose.services.applogin.UserSession;
import com.iw.dynamicglucose.services.user_profile.Measure;
import com.iw.dynamicglucose.services.user_profile.UserProfile;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class EditProfile extends AppCompatActivity {
    private Services services;
    private UserSession user;
    private CircleImageView img;
    private EditText name;
    private EditText age;
    private ListView lista;

    //Variable para la creacion de calendario
    Calendar myCalendar = Calendar.getInstance();

    //Dialogo para progresso de imagen
    ProgressDialog progressDialog;

    private final int MY_PERMISSION = 100;
    private final int PHOTO_CODE = 200;
    private final int SELECT_PICTURE = 300;

    //Variables de directorio para subir imegen
    private static String APP_DIRECTORY = "DG/";
    private static String MEDIA_DIRECTORY = APP_DIRECTORY + "Pictures";

    //Variable para configurar el mPath
    private String mPath = "";

    private Boolean settingsChanged = false;

    //loader
    private MaterialDialog dialogGetInfo;
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        getSupportActionBar().hide();

        //Inicializacionde variables
        Service service = new Service();
        services = service.getService();
        Session session = new Session(getBaseContext());
        user = session.getSession();
        img = findViewById(R.id.img_edit_profile);
        name = findViewById(R.id.input_name_edit_profile);
        age = findViewById(R.id.input_age_edit_profile);
        lista = findViewById(R.id.list_edit_profile);
        ImageButton back = findViewById(R.id.btn_back_edit_profile);
        Button save = findViewById(R.id.btn_save_edit_profile);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Uploading...");

        //Verificacion de permisos de camara y almacenamiento
        if(permisos()){
            img.setEnabled(true);
        }else{
            img.setEnabled(false);
        }

        //Funcion que obtiene la seleccion del calendario del usuario
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        //On click y despliegue de opciones de actualizacion de imagen
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showOptions();
            }
        });

        //On click que realiza la apertura del calendario
        age.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(EditProfile.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                settingsChanged = true;
            }
        });
        age.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                settingsChanged = true;
            }
        });


        //Back button
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AdapterEditProfile adapter = (AdapterEditProfile) lista.getAdapter();

                settingsChanged |= adapter.edited;

                if (settingsChanged){
                    final AlertDialog.Builder builder = new AlertDialog.Builder(EditProfile.this);
                    builder.setTitle("Do you want to save?")
                            .setMessage("Your profile has changes that are not saved")
                            .setPositiveButton("Save", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            saveChangesAndExit();
                        }
                    }).setNegativeButton("Disregard", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    }).setCancelable(true)
                            .show();
                }else {
                    finish();
                }
            }
        });


        //Funcion que obtiene los datos y hace la peticion a ws para hacer UPDATE
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveChangesAndExit();
            }
        });

        loadProfile();
    }
    private void saveChangesAndExit(){
        closeKeyboard();
        ArrayList<Map<String,String>> melements = new ArrayList<>();
        DataEditProfile data = new DataEditProfile();
        data.setPatient_id(user.getId());
        if(name.getText().toString().equals("")){
            data.setFull_name(name.getHint().toString());
        }else{
            data.setFull_name(name.getText().toString());
        }
        if(age.getText().toString().equals("")){
            data.setBirth_date(age.getHint().toString());
        }else{
            data.setBirth_date(age.getText().toString());
        }
        for(int i = 0; i<lista.getAdapter().getCount(); i++){
            Measure m = (Measure) lista.getAdapter().getItem(i);
            switch (m.getName()) {
                case "Weight":
                    data.setWeight(m.getValue());
                    break;
                case "Height":
                    data.setHeight(m.getValue());
                    break;
                default:
                    Map<String, String> b = new HashMap<>();
                    b.put(m.getName(), m.getValue());
                    melements.add(b);
                    break;
            }
        }
        data.setMeasures(melements);
        updateProfile(data);
    }
    //Revision de permisos
    private boolean permisos() {
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.M){
            return true;
        }
        if((checkSelfPermission(WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) && (checkSelfPermission(CAMERA) == PackageManager.PERMISSION_GRANTED)){
            return true;
        }
        if((shouldShowRequestPermissionRationale(WRITE_EXTERNAL_STORAGE)) || (shouldShowRequestPermissionRationale(CAMERA))){
            createToast("Missing Permisions");
        }else{
            requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE, CAMERA}, MY_PERMISSION);
        }
        return false;
    }

    //Funcion para cerrar el teclado
    private void closeKeyboard(){
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    //Funcion que despliega el modal para la seleccion de galeria o camara
    private void showOptions() {
        final CharSequence[] opciones = {"Photo", "Choose from gallery", "Cancel"};
        final AlertDialog.Builder builder = new AlertDialog.Builder(EditProfile.this);
        builder.setTitle("Pick an option");
        builder.setItems(opciones, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(opciones[which] == "Photo"){
                    abrirCamara();
                    settingsChanged = true;
                }else if(opciones[which] == "Choose from gallery"){
                    Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    i.setType("image/*");
                    startActivityForResult(Intent.createChooser(i, "Selecciona app de imagen"), SELECT_PICTURE);
                    settingsChanged = true;
                }else{
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    //Funcion que hace la llamada para cargar infomracion actual del usuario
    private void loadProfile(){
        dialogGetInfo = loader();
        dialogGetInfo.show();
        // Create the call of the service view program progress
        Call<UserProfile> call = services.get_user_profile(user.getId());

        // Executing call of the service view program progress
        //progressBarGet.setVisibility(View.VISIBLE);
        call.enqueue(new Callback<UserProfile>() {
            @Override
            public void onResponse(Call<UserProfile> call, Response<UserProfile> response) {
                switch (response.code()) {
                    case 200:
                        UserProfile data = response.body();

                        assert data != null;
                        name.setHint(data.getFullName());
                        age.setHint(data.getAge());
                        if (!data.getAvatar().equals("")){
                            Picasso.with(getApplicationContext()).load(data.getAvatar()).into(img);
                        }
                        AdapterEditProfile adapter = new AdapterEditProfile(getApplicationContext(), data.getMeasure());
                        lista.setAdapter(adapter);

                        break;
                    default:

                        break;
                }
                dialogGetInfo.dismiss();
            }

            @Override
            public void onFailure(Call<UserProfile> call, Throwable t) {
                //progressBarGet.setVisibility(View.GONE);
                createToast("Error: " + t.getMessage());
                dialogGetInfo.dismiss();
            }
        });
    }

    //Funcion para abrir camara
    private void abrirCamara() {
        File file = new File(Environment.getExternalStorageDirectory(), MEDIA_DIRECTORY);
        boolean isDirectoryCreated = file.exists();
        if(!isDirectoryCreated){
            isDirectoryCreated = file.mkdirs();
        }
        if(isDirectoryCreated){
            Long timestamp = System.currentTimeMillis() / 1000;
            String imgeName = timestamp.toString() + ".jpg";
            mPath = Environment.getExternalStorageDirectory() + File.separator + MEDIA_DIRECTORY + File.separator + imgeName;
            File newFile = new File(mPath);
            Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            i.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile (EditProfile.this, getApplicationContext().getPackageName() + ".provider", newFile));
            startActivityForResult(i, PHOTO_CODE);
        }
    }

    //Funcion que realiza la paticion al servicio para actualizar el perfil
    private void  updateProfile(DataEditProfile info){
        // Create the call of the service view program progress
        Call<ResponseBody> call = services.update_profile(info);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                switch (response.code()) {
                    case 200:
                        createToast("Success");
                        finish();
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                createToast("Error saving changes\n" + t);
            }
        });
    }

    //Funcion para crear toast
    private void createToast(String msg){
        Toast toast = Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    //Funcion para crear un nuevo dialog
    private MaterialDialog loader(){
        MaterialDialog.Builder builder = new MaterialDialog.Builder(this)
                .content("Please wait...")
                .progress(true, 0);
        return builder.build();
    }

    //Actulizacion de la label de edad
    private void updateLabel() {
        String myFormat = "MM/dd/yy"; //In which you need put here
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-d");
        age.setText(sdf.format(myCalendar.getTime()));
    }

    // Uploading Image/Video
    private void uploadFile() {
        progressDialog.show();

        // Map is used to multipart the file using okhttp3.RequestBody
        File file = new File(mPath);

        Uri fileUri = Uri.parse(file.getPath());

        File fileUpload = com.ipaulpro.afilechooser.utils.FileUtils.getFile(fileUri);
        // Parsing any Media type file
        RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), fileUpload);
        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("avatar", fileUpload.getName(), requestBody);
//        RequestBody filename = RequestBody.create(MediaType.parse("text/plain"), user.getId().toString());;
        RequestBody patient_id = RequestBody.create(MultipartBody.FORM, user.getId().toString());

        Call<ResponseBody> call = services.uploadAvatar(patient_id, fileToUpload);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                switch (response.code()) {
                    case 200:
                        createToast("Success");
                        finish();
                        break;
                    default:
                        break;
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                createToast("error");
                progressDialog.dismiss();
            }
        });
    }

    //Funcion que maneja la imagen seleccionada o la imagen que se tomo con la camara
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK){
            switch (requestCode){
                case PHOTO_CODE:
                    Uri path1 = Uri.fromFile(new File(mPath));
                    img.setImageURI(path1);
                    uploadFile();
                    Log.i("URL", "Uri = " + mPath);
                    break;
                case SELECT_PICTURE:
                    Uri path2 = data.getData();
                    mPath = getRealPathFromURI(path2);
                    img.setImageURI(path2);
                    uploadFile();
                    break;
            }
        }
    }
    public String getRealPathFromURI(Uri uri) {
        @SuppressLint("Recycle") Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        assert cursor != null;
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    /**+
     * Funcion que muestra los permisos
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == MY_PERMISSION){
            if(grantResults.length == 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED){
                img.setEnabled(true);
            }
        }else{
            showExplanation();
        }
    }

    /**
     * funcion que muestra una explicacion de los permisos
     */
    private void showExplanation() {
        AlertDialog.Builder builder = new AlertDialog.Builder(EditProfile.this);
        builder.setTitle("Permisos denegados");
        builder.setMessage("Para poder cambiar la imagen del menu necesitas aceptar los permisos");
        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent i = new Intent();
                i.setAction(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getPackageName(), null);
                i.setData(uri);
                startActivity(i);
            }
        });
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }
}
