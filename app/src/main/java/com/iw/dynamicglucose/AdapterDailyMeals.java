package com.iw.dynamicglucose;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.iw.dynamicglucose.ExtraObjects.OnItemClick;
import com.iw.dynamicglucose.services.get_daily_check_meals.Option;
import java.util.List;

/**
 * Created by IW on 21/02/2018.
 */

public class AdapterDailyMeals extends RecyclerView.Adapter<AdapterDailyMeals.ViewHolderDailyMeals>{
    private List<Option> getDayMeals;
    private Context context;
    private final OnItemClick listener;
    private Integer rPosition;

    AdapterDailyMeals(Context context, List<Option> getDayMeals, Integer rPosition, OnItemClick listener) {
        this.getDayMeals = getDayMeals;
        this.context = context;
        this.listener = listener;
        this.rPosition = rPosition;
    }
    @Override
    public AdapterDailyMeals.ViewHolderDailyMeals onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AdapterDailyMeals.ViewHolderDailyMeals(LayoutInflater.from(context).inflate(R.layout.cell_daily_meals, parent, false));
    }

    void deselectAll() {
        for (Option option : getDayMeals) {
            option.setDone(false);
        }
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(final AdapterDailyMeals.ViewHolderDailyMeals holder, int position) {
        Option getDayMeal = getDayMeals.get(position);
        holder.bind(getDayMeal, listener, context, position, rPosition);
    }

    @Override
    public int getItemCount() {
        return getDayMeals.size();
    }

    static class ViewHolderDailyMeals extends RecyclerView.ViewHolder{

        RelativeLayout content;
        ImageView img;
        TextView description;
        Integer position;
        Integer rPosition;
        Boolean done = false;
        ViewHolderDailyMeals(View itemView) {
            super(itemView);
            content = itemView.findViewById(R.id.container_daily);
            description = itemView.findViewById(R.id.text_description);
            img = itemView.findViewById(R.id.checked);
        }
        void bind(final Option item, final OnItemClick listener, final Context context, final Integer position, final Integer rPosition){
            StringBuilder txt = new StringBuilder();
            for(int i = 0; i< item.getName().size(); i++){
                txt.append(item.getName().get(i));

            }for(int i = 0; i< item.getRecipe().size(); i++){
                txt.append(item.getRecipe().get(i));

            }
            description.setText(txt.toString());
            this.position = position;
            this.rPosition = rPosition;
            if(item.getDone()){
                content.setBackground(context.getResources().getDrawable(R.drawable.rounded_daily_background_green));
                img.setVisibility(View.VISIBLE);
                description.setTextColor(context.getResources().getColor(R.color.white));
                done = true;
            }else{
                content.setBackground(context.getResources().getDrawable(R.drawable.rounded_daily_background_gray));
                img.setVisibility(View.GONE);
                description.setTextColor(context.getResources().getColor(R.color.purpleGray));
                done = false;
            }
            content.setOnClickListener(new View.OnClickListener() {
                @SuppressLint("ResourceAsColor")
                @Override
                public void onClick(View view) {
                    if(done){
                        content.setBackground(context.getResources().getDrawable(R.drawable.rounded_daily_background_gray));
                        img.setVisibility(View.GONE);
                        description.setTextColor(context.getResources().getColor(R.color.purpleGray));
                        done = false;
                    }else{
                        content.setBackground(context.getResources().getDrawable(R.drawable.rounded_daily_background_green));
                        img.setVisibility(View.VISIBLE);
                        description.setTextColor(context.getResources().getColor(R.color.white));
                        done = true;
                    }
                    listener.onItemClick(item, position, rPosition);
                }
            });
        }
    }
}
