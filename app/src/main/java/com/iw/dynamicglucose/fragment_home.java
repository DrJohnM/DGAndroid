package com.iw.dynamicglucose;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.iw.dynamicglucose.config.Service;
import com.iw.dynamicglucose.config.Session;
import com.iw.dynamicglucose.services.Services;
import com.iw.dynamicglucose.services.applogin.UserSession;
import com.iw.dynamicglucose.services.do_daily_check.DoDailyCheck;
import com.iw.dynamicglucose.services.get_day_meals.GetDayMeal;
import com.iw.dynamicglucose.services.get_day_mindsets_work.Activity;
import com.iw.dynamicglucose.services.get_day_mindsets_work.GetDayMindsetsWork;
import com.iw.dynamicglucose.services.view_progress_program.ViewProgramProgress;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class fragment_home extends Fragment {
    // TODO: Rename parameter arguments, choose names that match

    // Declare vars layout
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private LinearLayout layoutMindsetWorkout1;
    private LinearLayout layoutMindsetWorkout2;
    private ImageView imageMindsetWorkout1;
    private ImageView imageMindsetWorkout2;
    private TextView textTitleMindsetWoprkout1;
    private TextView textTitleMindsetWoprkout2;
    private TextView textNumberMindsetWoprkout1;
    private TextView textNumberMindsetWoprkout2;
    private TextView textTimeMindsetWoprkout1;
    private TextView txtDays;
    private BottomNavigationView bottomNavigationView;
    private LinearLayout myProgress;
    private LinearLayout myGroup;
    private LinearLayout Preferences;
    private LinearLayout DailyCheck;
    private RelativeLayout showMeals;
    private RelativeLayout hiddenMeals;
    private RelativeLayout showActivities;
    private RelativeLayout hiddenActivities;
    private RelativeLayout container_workout_home;
    private RelativeLayout container_mindset_home;

    private Services services;
    private Session session;
    private UserSession user;

    // Declare var progress dialog
    private  MaterialDialog dialogMeals;
    private  MaterialDialog dialogDays;
    private  MaterialDialog dialogMindWork;

    private ArrayList<Call<? extends  Object>> calls = new ArrayList<>();

    public fragment_home() {
    }

    public static fragment_home newInstance() {
        return new fragment_home();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_fragment_home, container, false);
    }
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        // Assignment vars config
        Service service = new Service();
        services = service.getService();
        session = new Session(getContext());
        user = session.getSession();

        // Assignment vars layout
        recyclerView = getView().findViewById(R.id.listView);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);
        layoutMindsetWorkout1 = getView().findViewById(R.id.lay_content_mind_work_1);
        layoutMindsetWorkout2 = getView().findViewById(R.id.lay_content_mind_work_2);
        imageMindsetWorkout1 = getView().findViewById(R.id.img_home_mind_work_1);
        imageMindsetWorkout2 = getView().findViewById(R.id.img_home_mind_work_2);
        textTitleMindsetWoprkout1 = getView().findViewById(R.id.txt_home_mind_work_title_1);
        textTitleMindsetWoprkout2 = getView().findViewById(R.id.txt_home_mind_work_title_2);
        textNumberMindsetWoprkout1 = getView().findViewById(R.id.txt_home_number_mind_work_1);
        textTimeMindsetWoprkout1 = getView().findViewById(R.id.txt_home_time_mind_work_1);
        container_workout_home = getView().findViewById(R.id.container_workout_home);
        container_mindset_home = getView().findViewById(R.id.container_mindset_home);
        txtDays = getView().findViewById(R.id.lbl_days);
        bottomNavigationView = ((Home)getActivity()).findViewById(R.id.navigation);
        myProgress = getView().findViewById(R.id.btn_my_progress);
        myGroup = getView().findViewById(R.id.btn_my_group);
        Preferences = getView().findViewById(R.id.btn_preference);
        showMeals = getView().findViewById(R.id.show_meals);
        hiddenMeals = getView().findViewById(R.id.hidden_meals);
        showActivities = getView().findViewById(R.id.show_activities);
        hiddenActivities = getView().findViewById(R.id.hidden_activities);
        DailyCheck = getView().findViewById(R.id.btn_daily_check);

        if(session.getCodeNotif() == 0){
        }else if (session.getCodeNotif() == 1){
            fragment_progress fragment = fragment_progress.newInstance();
            FragmentTransaction ft = ((FragmentActivity)getContext()).getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.frame_layout, fragment);
            ft.commit();
        }else if (session.getCodeNotif() == 2){
            fragment_progress fragment = fragment_progress.newInstance();
            FragmentTransaction ft = ((FragmentActivity)getContext()).getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.frame_layout, fragment);
            ft.commit();
        }else if (session.getCodeNotif() == 3){
            fragment_meals fragment = fragment_meals.newInstance();
            FragmentTransaction ft = ((FragmentActivity)getContext()).getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.frame_layout, fragment);
            ft.commit();
        }else if (session.getCodeNotif() == 4){
            fragment_workouts fragment = fragment_workouts.newInstance();
            FragmentTransaction ft = ((FragmentActivity)getContext()).getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.frame_layout, fragment);
            ft.commit();
        }else if (session.getCodeNotif() == 5){
            Intent i = new Intent(getContext(), ChatRoom.class);
            startActivity(i);
        }
        if(session.getCodeNotif() != 0){
            session.setCodeNotif(0);
        }
        loadProgramProgress();
        loadMeals();
        loadWorkoutMindset();
        loadOnClickBtn();
        registerAccess();
        doDailyCheck();
    }
    private void registerAccess(){
        Call<ResponseBody> call = services.send_home_access(user.getId());

        // Executing call of the service view program progress
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                switch (response.code()){
                    case 200:
                        break;
                    default:
                        MaterialDialog.Builder dialog = createMessage();
                        dialog.build();
                        dialog.show();
                        break;
                }
                dialogDays.dismiss();
                calls.remove(call);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                calls.remove(call);
            }
        });
        calls.add(call);
    }
    private void loadOnClickBtn() {
        myGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getContext(), ChatRoom.class);
                startActivity(i);
            }
        });


        Preferences.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getContext(), Settings.class);
                startActivity(i);
            }
        });
    }
    private void doDailyCheck(){
        Call<DoDailyCheck> call = services.do_daily_check(user.getId());
        call.enqueue(new Callback<DoDailyCheck>() {
            @Override
            public void onResponse(Call<DoDailyCheck> call, Response<DoDailyCheck> response) {
                switch (response.code()){
                    case 200:
                        assert response.body() != null;
                        if(response.body().getDaily_check()){
                            DailyCheck.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Intent i = new Intent(getContext(), DailyCheckMeals.class);
                                    startActivity(i);
                                }
                            });
                        }else {
                            DailyCheck.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Toast toast = Toast.makeText(getContext(), "Daily checkout has already been done today", Toast.LENGTH_LONG);
                                    toast.setGravity(Gravity.CENTER, 0, 0);
                                    toast.show();
                                }
                            });
                        }
                        assert response.body() != null;
                        //if(response.body().getWeigh_in()){
                            myProgress.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    fragment_progress fragment = fragment_progress.newInstance();
                                    FragmentTransaction ft = ((FragmentActivity)getContext()).getSupportFragmentManager().beginTransaction();
                                    ft.replace(R.id.frame_layout, fragment);
                                    ft.commit();
                                }
                            });
                        //}

                        break;
                    default:
                        break;
                }
                calls.remove(call);
            }

            @Override
            public void onFailure(Call<DoDailyCheck> call, Throwable t) {
                calls.remove(call);
            }
        });
        calls.add(call);
    }
    private void loadProgramProgress(){
        dialogDays = loader();
        dialogDays.show();
        // Create the call of the service view program progress
        Call<ViewProgramProgress> call = services.view_program_progress(user.getId());

        // Executing call of the service view program progress
        call.enqueue(new Callback<ViewProgramProgress>() {
            @Override
            public void onResponse(Call<ViewProgramProgress> call, Response<ViewProgramProgress> response) {
                switch (response.code()){
                    case 200:
                        ViewProgramProgress data = response.body();
                        if(data.getIs_active()){
                            txtDays.setText(data.getProgress_program());
                        }else{
                            session.CloseSession();
                            Intent i = new Intent(getContext(), Login.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            startActivity(i);
                            getActivity().finish();
                        }

                        break;
                    default:
                        MaterialDialog.Builder dialog = createMessage();
                        dialog.build();
                        dialog.show();
                        break;
                }
                dialogDays.dismiss();
                calls.remove(call);
            }

            @Override
            public void onFailure(Call<ViewProgramProgress> call, Throwable t) {
                dialogDays.dismiss();
                MaterialDialog.Builder dialog = createMessage();
                if ( dialog != null ) {
                    dialog.build();
                    dialog.show();
                }
                calls.remove(call);
            }
        });
        calls.add(call);
    }

    private void loadMeals(){
        dialogMeals = loader();
        dialogMeals.show();
        // Create the call of the service get day meals
        Call<ArrayList<GetDayMeal>> call = services.get_day_meals(user.getId());

        // Executing call of the service get day meals
        call.enqueue(new Callback<ArrayList<GetDayMeal>>() {

            @Override
            public void onResponse(Call<ArrayList<GetDayMeal>> call, Response<ArrayList<GetDayMeal>> response) {
                switch (response.code()){
                    case 200:
                        // Getting data objects
                        ArrayList<GetDayMeal> data = response.body();
                        adapter = new AdapterHomeMeals(getContext(), data, bottomNavigationView);
                        recyclerView.setAdapter(adapter);
                        break;

                    case 204:
                        showMeals.setVisibility(View.GONE);
                        hiddenMeals.setVisibility(View.VISIBLE);
                        break;
                    default:
                        MaterialDialog.Builder dialog = createMessage();
                        dialog.build();
                        dialog.show();
                        break;
                }
                dialogMeals.dismiss();
                calls.remove(call);
            }

            @Override
            public void onFailure(Call<ArrayList<GetDayMeal>> call, Throwable t) {
                dialogMeals.dismiss();
                MaterialDialog.Builder dialog = createMessage();
                if ( dialog != null ) {
                    dialog.build();
                    dialog.show();
                }
                calls.remove(call);
            }


        });
        calls.add(call);
    }

    private void loadWorkoutMindset(){
        dialogMindWork = loader();
        dialogMindWork.show();
        // Create the call of the service get day mindsets work
        Call<GetDayMindsetsWork> call = services.get_day_mindsets_work(user.getId());

        // Executing call of the service get day mindsets work
        call.enqueue(new Callback<GetDayMindsetsWork>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<GetDayMindsetsWork> call, Response<GetDayMindsetsWork> response) {
                switch(response.code()){
                    case 200:
                        GetDayMindsetsWork data = response.body();
                        assert data != null;
                        if(data.getActivities().size() == 1){
                            if(data.getActivities().get(0).getType().equals("workout")){
                                UserSession change_mindset = session.getSession();
                                change_mindset.setShow_mindsets(false);
                                Session s = new Session(change_mindset, getContext());
                                s.CreateSession();
                                container_mindset_home.setVisibility(View.INVISIBLE);
                                textTitleMindsetWoprkout2.setVisibility(View.INVISIBLE);
                                Activity exercise1 = data.getActivities().get(0);
                                layoutMindsetWorkout1.setVisibility(View.VISIBLE);
                                Picasso.with(getContext()).load(exercise1.getImage()).into(imageMindsetWorkout1);
                                textTitleMindsetWoprkout1.setText(exercise1.getType().toUpperCase());
                                if (!exercise1.getTimeString().equals("") || exercise1.getTimeString() != null) {
                                    String[] time = exercise1.getTimeString().split(" ");
                                    String[] number = time[0].split("\\.");
                                    textNumberMindsetWoprkout1.setText(number[0]);
                                    textTimeMindsetWoprkout1.setText(time[1] + "s");
                                }
                                layoutMindsetWorkout1.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        MenuItem item = bottomNavigationView.getMenu().getItem(2);
                                        item.setChecked(true);
                                        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                                        transaction.replace(R.id.frame_layout, fragment_workouts.newInstance());
                                        transaction.commit();
                                    }
                                });

                            }else{
                                container_workout_home.setVisibility(View.INVISIBLE);
                                textTitleMindsetWoprkout1.setVisibility(View.INVISIBLE);
                                Activity exercise2 = data.getActivities().get(0);
                                layoutMindsetWorkout2.setVisibility(View.VISIBLE);
                                Picasso.with(getContext()).load(exercise2.getImage()).into(imageMindsetWorkout2);
                                textTitleMindsetWoprkout2.setText(exercise2.getType().toUpperCase());
                                if (!exercise2.getTimeString().equals("") || exercise2.getTimeString() != null) {
                                    String[] time = exercise2.getTimeString().split(" ");
                                    String[] number = time[0].split("\\.");
                                }

                                layoutMindsetWorkout2.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        MenuItem item = bottomNavigationView.getMenu().getItem(3);
                                        item.setChecked(true);
                                        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                                        transaction.replace(R.id.frame_layout, fragment_mindset.newInstance());
                                        transaction.commit();
                                    }
                                });

                            }
                        }else{
                            UserSession change_mindset = session.getSession();
                            change_mindset.setShow_mindsets(true);
                            Session s = new Session(change_mindset, getContext());
                            s.CreateSession();
                            for(int i = 0; i < data.getActivities().size(); i++){

                                if(data.getActivities().size() > 0){
                                    switch (i) {
                                        case 0:
                                            Activity exercise1 = data.getActivities().get(i);
                                            layoutMindsetWorkout1.setVisibility(View.VISIBLE);
                                            Picasso.with(getContext()).load(exercise1.getImage()).into(imageMindsetWorkout1);
                                            textTitleMindsetWoprkout1.setText(exercise1.getType().toUpperCase());
                                            if (!exercise1.getTimeString().equals("") || exercise1.getTimeString() != null) {
                                                String[] time = exercise1.getTimeString().split(" ");
                                                String[] number = time[0].split("\\.");
                                                textNumberMindsetWoprkout1.setText(number[0]);
                                                textTimeMindsetWoprkout1.setText(time[1] + "s");
                                            }
                                            if(exercise1.getType().equals("workout")){
                                                layoutMindsetWorkout1.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View view) {
                                                        MenuItem item = bottomNavigationView.getMenu().getItem(2);
                                                        item.setChecked(true);
                                                        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                                                        transaction.replace(R.id.frame_layout, fragment_workouts.newInstance());
                                                        transaction.commit();
                                                    }
                                                });
                                            }else{
                                                layoutMindsetWorkout1.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View view) {
                                                        MenuItem item = bottomNavigationView.getMenu().getItem(3);
                                                        item.setChecked(true);
                                                        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                                                        transaction.replace(R.id.frame_layout, fragment_mindset.newInstance());
                                                        transaction.commit();
                                                    }
                                                });
                                            }
                                            break;
                                        case 1:
                                            Activity exercise2 = data.getActivities().get(i);
                                            layoutMindsetWorkout2.setVisibility(View.VISIBLE);
                                            Picasso.with(getContext()).load(exercise2.getImage()).into(imageMindsetWorkout2);
                                            textTitleMindsetWoprkout2.setText(exercise2.getType().toUpperCase());
                                            if (!exercise2.getTimeString().equals("") || exercise2.getTimeString() != null) {
                                                String[] time = exercise2.getTimeString().split(" ");
                                                String[] number = time[0].split("\\.");
                                            }
                                            if(exercise2.getType().equals("workout")){
                                                layoutMindsetWorkout2.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View view) {
                                                        MenuItem item = bottomNavigationView.getMenu().getItem(2);
                                                        item.setChecked(true);
                                                        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                                                        transaction.replace(R.id.frame_layout, fragment_workouts.newInstance());
                                                        transaction.commit();
                                                    }
                                                });
                                            }else{
                                                layoutMindsetWorkout2.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View view) {
                                                        MenuItem item = bottomNavigationView.getMenu().getItem(3);
                                                        item.setChecked(true);
                                                        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                                                        transaction.replace(R.id.frame_layout, fragment_mindset.newInstance());
                                                        transaction.commit();
                                                    }
                                                });
                                            }
                                            break;
                                    }
                                }

                            }
                        }

                        break;

                    case 204:
                        UserSession change_mindset = session.getSession();
                        change_mindset.setShow_mindsets(false);
                        Session s = new Session(change_mindset, getContext());
                        s.CreateSession();
                        showActivities.setVisibility(View.GONE);
                        hiddenActivities.setVisibility(View.VISIBLE);
                        break;
                    default:
                        MaterialDialog.Builder dialog = createMessage();
                        dialog.build();
                        dialog.show();
                    break;
                }
                dialogMindWork.dismiss();
                calls.remove(call);
            }

            @Override
            public void onFailure(Call<GetDayMindsetsWork> call, Throwable t) {
                dialogMindWork.dismiss();
                MaterialDialog.Builder dialog = createMessage();
                if ( dialog != null ) {
                    dialog.build();
                    dialog.show();
                }
                calls.remove(call);
            }
        });
        calls.add(call);
    }

    private MaterialDialog.Builder createMessage(){
        Context context = getContext();
        if (context == null) {
            return null;
        }
        return new MaterialDialog.Builder(context)
                .title("Error")
                .content("Server not available, try again later")
                .positiveText("Accept");
    }

    private MaterialDialog loader(){
        Context context = getContext();
        if (context == null) {
            return null;
        }
        MaterialDialog.Builder builder = new MaterialDialog.Builder(context)
                .content("Please wait...")
                .progress(true, 0);
        return builder.build();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        //cancel requests that are still executing
        for (Call call : calls) {
            call.cancel();
        }
    }
}
