package com.iw.dynamicglucose;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.iw.dynamicglucose.config.BaseUrl;
import com.iw.dynamicglucose.config.Service;
import com.iw.dynamicglucose.config.Session;
import com.iw.dynamicglucose.services.Services;
import com.iw.dynamicglucose.services.applogin.UserSession;
import com.iw.dynamicglucose.services.process_workout.ProcessWorkout;
import com.iw.dynamicglucose.services.view_progress_program.ViewProgramProgress;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class fragment_single_workout2 extends Fragment {

    //Layout vars
    private TextView title;
    private TextView time;
    private ImageButton rate1;
    private ImageButton rate2;
    private ImageButton rate3;
    private ImageButton rate4;
    private ImageButton rate5;
    private TextView lbl_rate;
    private Button markDone;
    private String id_workout;
    private TextView content;
    private TextView txtDays;
    private ImageView back_button_meals;
    private Boolean buttonSet;
    private Call doneCall;
    private int _rating = 0;

    //config vars
    private BaseUrl baseUrl;
    private Services services;
    private Session session;
    private UserSession user;

    private MaterialDialog dialogDays;

    public static fragment_single_workout2 newInstance(Bundle arguments){
        fragment_single_workout2 f = new fragment_single_workout2();
        if(arguments != null){
            f.setArguments(arguments);
        }
        return f;
    }

    public fragment_single_workout2() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static fragment_single_workout2 newInstance() {
        return new fragment_single_workout2();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        title = getView().findViewById(R.id.txt_title_single_workout2);
        time = getView().findViewById(R.id.txt_time_single_workout2);
        rate1 = getView().findViewById(R.id.btn_rate_1_single_workout2);
        rate2 = getView().findViewById(R.id.btn_rate_2_single_workout2);
        rate3 = getView().findViewById(R.id.btn_rate_3_single_workout2);
        rate4 = getView().findViewById(R.id.btn_rate_4_single_workout2);
        rate5 = getView().findViewById(R.id.btn_rate_5_single_workout2);
        lbl_rate = getView().findViewById(R.id.txt_rate_single_workout2);
        markDone = getView().findViewById(R.id.btn_markDone_single_workout2);
        content = getView().findViewById(R.id.txt_content_single_workout2);
        back_button_meals = getView().findViewById(R.id.back_button_meals);
        txtDays = getView().findViewById(R.id.lbl_days);

        id_workout = getArguments().getString("id");

        // Assignment vars config
        Service service = new Service();
        services = service.getService();
        session = new Session(getContext());
        user = session.getSession();

        if(session.getCodeNotif() != 0){
            session.setCodeNotif(0);
        }

        loadProgramProgress();

        markDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                markAsDone(user.getId(), Integer.parseInt(id_workout));
            }
        });
        rate1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rate(user.getId(), Integer.parseInt(id_workout), 1);
            }
        });
        rate2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rate(user.getId(), Integer.parseInt(id_workout), 2);
            }
        });
        rate3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rate(user.getId(), Integer.parseInt(id_workout), 3);
            }
        });
        rate4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rate(user.getId(), Integer.parseInt(id_workout), 4);
            }
        });
        rate5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rate(user.getId(), Integer.parseInt(id_workout), 5);
            }
        });
        loadWorkouts(Integer.parseInt(id_workout),user.getId());

        back_button_meals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment selectedFragment = null;
                selectedFragment = fragment_workouts.newInstance();
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_layout, selectedFragment);
                transaction.commit();
            }
        });
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fragment_single_workout2, container, false);
    }
    private void rate(Integer patient_id, Integer workout_id, final Integer rating){
        final int oldRate = _rating;
        setRate(rating);

        // Create the call of the service time to start
        Call<ResponseBody> call = services.set_rating_workout(patient_id,workout_id,rating);

        // Executing call of the service time to start
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() != 200) {
                    setRate(oldRate);
                }
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                MaterialDialog.Builder dialog = createMessage();
                dialog.build();
                dialog.show();
                setRate(oldRate);
            }
        });
    }
    private void markAsDone(Integer patient_id, Integer workout_id){
        final Boolean previous = buttonSet;
        setButton(!previous);

        // Create the call of the service time to start
        Call<ResponseBody> call = services.set_progress_workout(patient_id,workout_id, buttonSet?"true":"false");
        if(doneCall != null){
            doneCall.cancel();
        }
        doneCall = call;

        // Executing call of the service time to start
        call.enqueue(new Callback<ResponseBody>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() != 200) {
                    setButton(previous);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (!call.isCanceled()) {
                    setButton(previous);
                    Log.e("errorCall", t.getMessage());
                }
            }
        });

    }
    private void setRate(Integer rating) {
        _rating = rating;
        ImageButton[] rates = {rate1, rate2, rate3, rate4, rate5};
        for (ImageButton rate: rates) {
            rate.setImageResource(R.drawable.star);
        }
        for (int i=0; i<_rating; i++){
            rates[i].setImageResource(R.drawable.star_checked);
        }
    }
    private void setButton(boolean done){
        if (done){
            markDone.setBackground(getResources().getDrawable(R.drawable.blue_checked_rounded_button));
            markDone.setTextColor(Color.WHITE);
            markDone.setText(getString(R.string.done));

        }else {
            markDone.setBackground(getResources().getDrawable(R.drawable.blue_rounded_button2));
            markDone.setTextColor(getResources().getColor(R.color.purpleGray));
            markDone.setText(getString(R.string.markAsDone));
        }
        buttonSet = done;
    }

    private void loadWorkouts(Integer id_workout, Integer id_patient){
        final MaterialDialog loader = loader();
        loader.show();

        // Create the call of the service time to start
        Call<ProcessWorkout> call = services.process_workout(id_workout, id_patient);

        // Executing call of the service time to start
        call.enqueue(new Callback<ProcessWorkout>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<ProcessWorkout> call, Response<ProcessWorkout> response) {
                switch (response.code()){
                    case 200:
                        // Getting data objects
                        ProcessWorkout data = response.body();
                        // Set value to service response to label text days
                        assert data != null;
                        setButton(data.getDone());
                        content.setText(data.getDescription());
                        title.setText(data.getTitle());
                        time.setText(data.getDuration());
                        lbl_rate.setText(data.getRating().toString() + "/5");
                        setRate(data.getRating());
                        break;
                    default:
                        MaterialDialog.Builder dialog = createMessage();
                        dialog.build();
                        dialog.show();
                        break;
                }
                loader.dismiss();
            }

            @Override
            public void onFailure(Call<ProcessWorkout> call, Throwable t) {
                MaterialDialog.Builder dialog = createMessage();
                dialog.build();
                dialog.show();
                loader.dismiss();
                Log.e("Error", "call: " + t.getMessage());
            }


        });
    }

    private MaterialDialog.Builder createMessage(){
        return new MaterialDialog.Builder(getContext())
                .title("Error")
                .content("Server not available, try again later")
                .positiveText("Accept");
    }

    private MaterialDialog loader(){
        MaterialDialog.Builder builder = new MaterialDialog.Builder(getContext())
                .content("Please wait...")
                .progress(true, 0);
        return builder.build();
    }

    private void loadProgramProgress(){
        dialogDays = loader();
        dialogDays.show();
        // Create the call of the service view program progress
        Call<ViewProgramProgress> call = services.view_program_progress(user.getId());

        // Executing call of the service view program progress
        call.enqueue(new Callback<ViewProgramProgress>() {
            @Override
            public void onResponse(Call<ViewProgramProgress> call, Response<ViewProgramProgress> response) {
                switch (response.code()){
                    case 200:
                        ViewProgramProgress data = response.body();
                        assert data != null;
                        if(data.getIs_active()){
                            txtDays.setText(data.getProgress_program());
                        }else{
                            session.CloseSession();
                            Intent i = new Intent(getContext(), Login.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            startActivity(i);
                            getActivity().finish();
                        }

                        break;
                    default:
                        MaterialDialog.Builder dialog = createMessage();
                        dialog.build();
                        dialog.show();
                        break;
                }
                dialogDays.dismiss();
            }

            @Override
            public void onFailure(Call<ViewProgramProgress> call, Throwable t) {
                dialogDays.dismiss();
                MaterialDialog.Builder dialog = createMessage();
                dialog.build();
                dialog.show();
            }
        });
    }
}
