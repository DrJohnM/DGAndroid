package com.iw.dynamicglucose;

import android.annotation.SuppressLint;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.iw.dynamicglucose.ExtraObjects.DataMiscelaneus;
import com.iw.dynamicglucose.config.Service;
import com.iw.dynamicglucose.config.Session;
import com.iw.dynamicglucose.services.Services;
import com.iw.dynamicglucose.services.applogin.UserSession;
import com.iw.dynamicglucose.services.equipment.Equipment;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Miscelaneus extends AppCompatActivity {
    private Services services;
    private UserSession user;
    private ImageView img;
    private ListView lista;
    private TextView question;
    private TextView count;
    private Integer counter = 0;
    private Integer position = null;
    private Equipment questions;
    private ArrayList<Integer> answerids = new ArrayList<>();
    private Boolean flagSettings;

    //loader
    private MaterialDialog dialogLifestyle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_miscelaneus);

        getSupportActionBar().hide();

        flagSettings = getIntent().getBooleanExtra("settings", false);

        Service service = new Service();
        services = service.getService();
        Session session = new Session(getBaseContext());
        user = session.getSession();
        count = findViewById(R.id.txt_title_lifestyle);
        question = findViewById(R.id.txt_count_lifestyle);
        Button button = findViewById(R.id.btn_lifestyle);
        lista = findViewById(R.id.list_misc);
        img = findViewById(R.id.img_misc);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(position != null){
                    if(position == 0){
                        answerids.add(questions.getEquipments().get(counter).getId());
                    }
                    position = null;
                    if (counter == questions.getEquipments().size()-1){
                        DataMiscelaneus data = new DataMiscelaneus(user.getId(), answerids);
                        setLifstyle(data);
                    }else{
                        counter++;
                        chargeQuestion(counter);
                    }
                }else {
                    createToast("Please select an item");
                }
            }
        });
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                position = i;
            }
        });
        loadMisc();
    }

    private void loadMisc(){
        dialogLifestyle = loader();
        dialogLifestyle.show();
        // Create the call of the service view program progress
        Call<Equipment> call = services.get_equipment();

        // Executing call of the service view program progress
        //progressBar.setVisibility(View.VISIBLE);
        call.enqueue(new Callback<Equipment>() {
            @Override
            public void onResponse(Call<Equipment> call, Response<Equipment> response) {
                switch (response.code()) {
                    case 200:
                        questions  = response.body();
                        chargeQuestion(counter);
                        break;
                    default:

                        break;
                }
                dialogLifestyle.dismiss();
            }

            @Override
            public void onFailure(Call<Equipment> call, Throwable t) {
                dialogLifestyle.dismiss();
                createToast("Error: " + t.getMessage());
            }
        });
    }
    private void  setLifstyle(DataMiscelaneus info){
        // Create the call of the service view program progress
        Call<ResponseBody> call = services.set_equipment(info);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                switch (response.code()) {
                    case 200:
                        if (flagSettings){
                            finish();
                        }else {
                            Intent i = new Intent(getApplicationContext(), SplashScreen.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            createToast("Success");
                            startActivity(i);
                            finish();
                        }
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                createToast("Error" + t);
            }
        });
    }
    @SuppressLint("SetTextI18n")
    private void chargeQuestion(Integer counter){
        count.setText((counter +1) + " of " + questions.getEquipments().size());
        question.setText(questions.getEquipments().get(counter).getName());
        Picasso.with(getApplicationContext()).load(questions.getEquipments().get(counter).getPhoto()).into(img);
        lista.setAdapter(new AdapterMiscelaneaus(getApplicationContext()));
    }
    private void createToast(String msg){
        Toast toast = Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    private MaterialDialog loader(){
        MaterialDialog.Builder builder = new MaterialDialog.Builder(this)
                .content("Please wait...")
                .progress(true, 0);
        return builder.build();
    }
}
