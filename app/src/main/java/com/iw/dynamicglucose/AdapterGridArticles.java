package com.iw.dynamicglucose;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import androidx.cardview.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.iw.dynamicglucose.services.view_onboarding_articles.Article;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by IW on 16/feb/2018.
 */

public class AdapterGridArticles extends BaseAdapter {

    private LayoutInflater inflater;
    private List<Article> data;
    private Context context;
    private Boolean showReason;

    AdapterGridArticles(List<Article> data, Context context, Boolean showReason){
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.data = data;
        this.showReason = showReason;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return data.get(i);
    }

    @Override
    public long getItemId(int i) {
        return data.get(i).getId();
    }

    @SuppressLint({"ViewHolder", "InflateParams"})
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View article_view = inflater.inflate(R.layout.cell_onboarding_article, null);
        final Article article = data.get(i);

        CardView contentArticle = article_view.findViewById(R.id.article_content);
        ImageView imgArticle = article_view.findViewById(R.id.img_article);
        TextView txtArticle = article_view.findViewById(R.id.txt_article);

        Picasso.with(context).load(article.getImage()).into(imgArticle);
        txtArticle.setText(article.getTitle());

        contentArticle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(context, SingleArticle.class);
                i.putExtra("article_id", article.getId());
                i.putExtra("showReason", !showReason);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);
            }
        });

        return article_view;
    }
}
