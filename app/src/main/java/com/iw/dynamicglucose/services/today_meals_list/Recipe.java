
package com.iw.dynamicglucose.services.today_meals_list;


public class Recipe {

    private Integer recipeId;
    private String name;

    public Integer getRecipeId() {
        return recipeId;
    }

    public void setRecipeId(Integer recipeId) {
        this.recipeId = recipeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
