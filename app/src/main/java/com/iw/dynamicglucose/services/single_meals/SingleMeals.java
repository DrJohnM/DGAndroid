
package com.iw.dynamicglucose.services.single_meals;

import java.util.List;

public class SingleMeals {

    private List<Item> items = null;
    private Boolean done;
    private Integer mealTime;

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public Boolean getDone() {
        return done;
    }

    public void setDone(Boolean done) {
        this.done = done;
    }

    public Integer getMealTime() {
        return mealTime;
    }

    public void setMealTime(Integer mealTime) {
        this.mealTime = mealTime;
    }

}
