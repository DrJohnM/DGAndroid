
package com.iw.dynamicglucose.services.get_single_recipe;

import java.util.List;

public class GetSingleRecipe {

    private Integer rating;
    private String description;
    private Integer ratingAverage;
    private Boolean done;
    private List<String> nutritionalFacts = null;
    private String name;
    private List<Ingredient> ingredients = null;
    private String timePrep;
    private List<Step> steps = null;
    private Integer mealTime;
    private String photo;
    private String noServing;

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getRatingAverage() {
        return ratingAverage;
    }

    public void setRatingAverage(Integer ratingAverage) {
        this.ratingAverage = ratingAverage;
    }

    public Boolean getDone() {
        return done;
    }

    public void setDone(Boolean done) {
        this.done = done;
    }

    public List<String> getNutritionalFacts() {
        return nutritionalFacts;
    }

    public void setNutritionalFacts(List<String> nutritionalFacts) {
        this.nutritionalFacts = nutritionalFacts;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public String getTimePrep() {
        return timePrep;
    }

    public void setTimePrep(String timePrep) {
        this.timePrep = timePrep;
    }

    public List<Step> getSteps() {
        return steps;
    }

    public void setSteps(List<Step> steps) {
        this.steps = steps;
    }

    public Integer getMealTime() {
        return mealTime;
    }

    public void setMealTime(Integer mealTime) {
        this.mealTime = mealTime;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getNoServing() {
        return noServing;
    }

    public void setNoServing(String noServing) {
        this.noServing = noServing;
    }

}
