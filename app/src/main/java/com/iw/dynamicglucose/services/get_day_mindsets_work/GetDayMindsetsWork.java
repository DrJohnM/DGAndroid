
package com.iw.dynamicglucose.services.get_day_mindsets_work;

import java.util.List;

public class GetDayMindsetsWork {

    private List<Activity> activities = null;

    public List<Activity> getActivities() {
        return activities;
    }

    public void setActivities(List<Activity> activities) {
        this.activities = activities;
    }

}
