
package com.iw.dynamicglucose.services.lifestyle_video;

import java.util.List;

public class LifestyleVideo {

    private String question;
    private List<Answer> answers = null;
    private String file;

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public List<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Answer> answers) {
        this.answers = answers;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

}
