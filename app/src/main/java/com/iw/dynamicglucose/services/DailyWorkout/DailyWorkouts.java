
package com.iw.dynamicglucose.services.DailyWorkout;

import java.util.List;

public class DailyWorkouts {

    private List<Workout> workouts = null;

    public List<Workout> getWorkouts() {
        return workouts;
    }

    public void setWorkouts(List<Workout> workouts) {
        this.workouts = workouts;
    }

}
