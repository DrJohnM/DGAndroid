package com.iw.dynamicglucose.services.time_to_start;

/**
 * Created by IW on 30/ene/2018.
 */

public class TimeToStart {

    private Integer timeToStart;

    public TimeToStart(Integer timeToStart) {
        this.timeToStart = timeToStart;
    }

    public TimeToStart() {
    }

    public Integer getTimeToStart() {
        return timeToStart;
    }

    public void setTimeToStart(Integer timeToStart) {
        this.timeToStart = timeToStart;
    }
}
