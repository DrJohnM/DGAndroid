
package com.iw.dynamicglucose.services.get_single_recipe;


public class Step {

    private String step;

    public String getStep() {
        return step;
    }

    public void setStep(String step) {
        this.step = step;
    }

}
