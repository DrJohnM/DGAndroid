
package com.iw.dynamicglucose.services.workout_home;


public class WorkoutSec {

    private String image;
    private String title;
    private Boolean dayToDay;
    private Integer id;
    private String time;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getDayToDay() {
        return dayToDay;
    }

    public void setDayToDay(Boolean dayToDay) {
        this.dayToDay = dayToDay;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

}
