
package com.iw.dynamicglucose.services.today_meals_list;

import java.util.List;

public class TodayMealsList {

    private String mealTimeName;
    private String image;
    private Integer mealTimeId;
    private String mealTime;
    private List<Option> options = null;

    public String getMealTimeName() {
        return mealTimeName;
    }

    public void setMealTimeName(String mealTimeName) {
        this.mealTimeName = mealTimeName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getMealTimeId() {
        return mealTimeId;
    }

    public void setMealTimeId(Integer mealTimeId) {
        this.mealTimeId = mealTimeId;
    }

    public String getMealTime() {
        return mealTime;
    }

    public void setMealTime(String mealTime) {
        this.mealTime = mealTime;
    }

    public List<Option> getOptions() {
        return options;
    }

    public void setOptions(List<Option> options) {
        this.options = options;
    }

}
