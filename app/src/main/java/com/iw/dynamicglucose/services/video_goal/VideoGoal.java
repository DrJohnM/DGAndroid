
package com.iw.dynamicglucose.services.video_goal;


public class VideoGoal {

    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
