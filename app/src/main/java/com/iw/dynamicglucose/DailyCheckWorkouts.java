package com.iw.dynamicglucose;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.iw.dynamicglucose.ExtraObjects.DataDailyWorkouts;
import com.iw.dynamicglucose.config.Service;
import com.iw.dynamicglucose.config.Session;
import com.iw.dynamicglucose.services.DailyWorkout.DailyWorkouts;
import com.iw.dynamicglucose.services.DailyWorkout.Workout;
import com.iw.dynamicglucose.services.Services;
import com.iw.dynamicglucose.services.applogin.UserSession;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DailyCheckWorkouts extends AppCompatActivity {
    private ListView lista;
    private Services services;
    private UserSession user;
    private MaterialDialog dialogLifestyle;
    private ArrayList<Boolean> flags = new ArrayList<>();
    private ArrayList<Integer> ids = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_check_workouts);
        getSupportActionBar().hide();

        //Llamada de variables
        lista = findViewById(R.id.list_daily_workouts);
        Button next = findViewById(R.id.btm_daily_workouts);
        ImageButton close = findViewById(R.id.closeButtonDailyWorkouts);
        ImageButton back = findViewById(R.id.backButtonDailyWorkouts);
        Service service = new Service();
        services = service.getService();
        Session session = new Session(getBaseContext());
        user = session.getSession();

        //Seteo de propiedades
        lista.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        loadWorkoutse();

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (flags.get(i)){
                    view.setBackgroundColor(Color.parseColor("#ffffff"));
                    view.findViewById(R.id.daily_check_arrow).setVisibility(View.GONE);
                    flags.set(i, false);
                }else{
                    view.setBackgroundColor(Color.parseColor("#FEF1F2"));
                    view.findViewById(R.id.daily_check_arrow).setVisibility(View.VISIBLE);
                    flags.set(i, true);
                }
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            ArrayList<Integer> sendids = new ArrayList<>();
            public void onClick(View view) {
                for(Integer i = 0; i<flags.size(); i++){
                    if(flags.get(i)){
                        sendids.add(ids.get(i));
                    }
                }
                DataDailyWorkouts info = new DataDailyWorkouts(user.getId(), sendids);
                setWorkoutse(info);
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), Home.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(i);
                finish();
            }
        });
    }

    private void loadWorkoutse(){
        dialogLifestyle = loader();
        dialogLifestyle.show();
        // Create the call of the service view program progress
        Call<DailyWorkouts> call = services.get_daily_workouts(user.getId());

        // Executing call of the service view program progress
        call.enqueue(new Callback<DailyWorkouts>() {
            @Override
            public void onResponse(Call<DailyWorkouts> call, Response<DailyWorkouts> response) {
                switch (response.code()) {
                    case 200:
                        DailyWorkouts data = response.body();
                        //progressBar.setVisibility(View.GONE);
                        assert data != null;
                        lista.setAdapter(new AdapterDailyWorkouts(getApplicationContext(),data.getWorkouts()));
                        for(Workout wor: data.getWorkouts()){
                            ids.add(wor.getId());
                            if (wor.getDone()){
                                flags.add(true);
                            }else{
                                flags.add(false);
                            }
                        }
                        break;
                    default:

                        break;
                }
                dialogLifestyle.dismiss();
            }

            @Override
            public void onFailure(Call<DailyWorkouts> call, Throwable t) {
                //progressBar.setVisibility(View.GONE);
                dialogLifestyle.dismiss();
                createToast("Error: " + t.getMessage());
            }
        });
    }
    private void  setWorkoutse(DataDailyWorkouts info){
        dialogLifestyle = loader();
        dialogLifestyle.show();
        // Create the call of the service view program progress
        Call<ResponseBody> call = services.set_daily_workouts(info);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                switch (response.code()) {
                    case 200:
                        Intent i = new Intent(getApplicationContext(), DailyCheckoutGral.class);
                        startActivity(i);
                        break;
                    default:
                        break;
                }
                dialogLifestyle.dismiss();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                createToast("Error" + t);
                dialogLifestyle.dismiss();
            }
        });
    }
    private void createToast(String msg){
        Toast toast = Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    private MaterialDialog loader(){
        MaterialDialog.Builder builder = new MaterialDialog.Builder(this)
                .content("Please wait...")
                .progress(true, 0);
        return builder.build();
    }
}
