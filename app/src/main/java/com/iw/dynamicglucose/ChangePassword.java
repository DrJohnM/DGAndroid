package com.iw.dynamicglucose;

        import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.iw.dynamicglucose.config.Service;
import com.iw.dynamicglucose.config.Session;
import com.iw.dynamicglucose.services.Services;
import com.iw.dynamicglucose.services.applogin.UserSession;
import com.iw.dynamicglucose.services.change_pass.UpdatePass;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePassword extends AppCompatActivity {

    private Services services;
    private Session session;
    private UserSession user;
    private EditText oldPassword;
    private EditText newPassword1;
    private EditText newPassword2;

    private MaterialDialog dialogDays;
    private Boolean settingsChanged = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        getSupportActionBar().hide();

        Service service = new Service();
        services = service.getService();
        session = new Session(getBaseContext());
        user = session.getSession();

        oldPassword = findViewById(R.id.input_old_pass);
        newPassword1 = findViewById(R.id.input_new_pass);
        newPassword2 = findViewById(R.id.input_new_pass2);
        Button save = findViewById(R.id.btn_save_change_pass);
        ImageButton back = findViewById(R.id.btn_back_change_pass);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(newPassword1.getText().toString().equals("") || newPassword2.getText().toString().equals("") || oldPassword.getText().toString().equals("")){
                    createToast("Pleaser enter all fields");
                }else{
                    if(newPassword1.getText().toString().equals(newPassword2.getText().toString())){
                        if(isValidPassword(newPassword1.getText().toString().trim())){
                            updatePass(oldPassword.getText().toString(), newPassword1.getText().toString(), user.getId());
                        }else{
                            createToast("Please use at least 6 caracters, one number, one lower case and one upper case");
                        }

                    }else{
                        createToast("New password doesnt match");
                    }
                }
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }
    private void  updatePass(String oldPass, String newPass, Integer id){
        dialogDays = loader();
        dialogDays.show();
        // Create the call of the service view program progress
        Call<UpdatePass> call = services.update_password(id, newPass, oldPass);

        // Executing call of the service time to start
        call.enqueue(new Callback<UpdatePass>() {
            @Override
            public void onResponse(Call<UpdatePass> call, Response<UpdatePass> response) {
                switch (response.code()) {
                    case 200:
                        UpdatePass data = response.body();
                        assert data != null;
                        if(data.getErrors().equals("0")){
                            //setPassProgess.setVisibility(View.GONE);
                            createToast("Success");
                            session.CloseSession();
                            Intent i = new Intent(getApplicationContext(), Login.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            startActivity(i);
                            finish();
                        }else{
                            //setPassProgess.setVisibility(View.GONE);
                            createToast(data.getErrors());
                        }
                        dialogDays.dismiss();
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onFailure(Call<UpdatePass> call, Throwable t) {
                dialogDays.dismiss();
                createToast("Error" + t);
            }
        });
    }

    private MaterialDialog loader(){
        MaterialDialog.Builder builder = new MaterialDialog.Builder(this)
                .content("Please wait...")
                .progress(true, 0);
        return builder.build();
    }

    private void createToast(String msg){
        Toast toast = Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }
    public boolean isValidPassword(final String password) {

        Pattern pattern;
        Matcher matcher;

        final String PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{6,}$";

        pattern = Pattern.compile(PATTERN);
        matcher = pattern.matcher(password);

        return matcher.matches();

    }
}
