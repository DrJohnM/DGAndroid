package com.iw.dynamicglucose;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.halilibo.bvpkotlin.*;
import com.iw.dynamicglucose.config.Service;
import com.iw.dynamicglucose.config.Session;
import com.iw.dynamicglucose.services.Services;
import com.iw.dynamicglucose.services.applogin.UserSession;
import com.iw.dynamicglucose.services.view_single_article.ViewSingleArticle;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SingleArticle extends AppCompatActivity implements VideoCallback {

    // Declare other vars
    private Integer article_id;

    // Declare vars layout
    private CircleImageView backButton;
    private RelativeLayout mediaTop;
    private TextView title;
    private TextView content;
    private EditText editText;
    private RelativeLayout container_goal;
    //loaders
    private MaterialDialog dialogGetVideo;
    //private VideoView videoView;
    private BetterVideoPlayer player;

    private Services services;
    private UserSession user;

    //loader
    private MaterialDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_article);
        getSupportActionBar().hide();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        // Get article id from after intent
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                finish();
            } else {
                article_id = extras.getInt("article_id");
            }
        } else {
            finish();
        }

        // Assignment vars layout
        backButton = findViewById(R.id.btn_back);
        mediaTop = findViewById(R.id.media_top);
        title = findViewById(R.id.title);
        content = findViewById(R.id.content);
        container_goal = findViewById(R.id.container_goal);
        //videoView = findViewById(R.id.video_top);
        player = findViewById(R.id.player);
        player.setCallback(this);
        player.setAutoPlay(true);
        editText = findViewById(R.id.input_goal);
        Button button = findViewById(R.id.btn_goal);

        // Assignment vars config
        Service service = new Service();
        services = service.getService();
        Session session = new Session(getBaseContext());
        user = session.getSession();

        // Load screen
        loadArticle();

        // Set listeners
        setListeners();

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(editText.getText().toString().equals("")){
                    createToast("Please enter your real reason");
                }else{
                    setGoal(editText.getText().toString());
                }

            }
        });

    }

    private void setListeners() {
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    public void loadArticle(){
        dialog = loader();
        dialog.show();
        // Create the call of the view single article
        Call<ViewSingleArticle> call = services.view_single_article(article_id);

        // Executing call of the view single article
        call.enqueue(new Callback<ViewSingleArticle>() {
            @Override
            public void onResponse(Call<ViewSingleArticle> call, Response<ViewSingleArticle> response) {
                switch(response.code()){
                    case 200:
                        ViewSingleArticle data = response.body();
                        assert data != null;
                        if(!data.getMedia().equals("")){
                            //MediaController mediaController = new MediaController(getApplicationContext());
                            //mediaController.setAnchorView(videoView);
                            //videoView.setVideoURI(Uri.parse(data.getMedia()));
                            //videoView.start();
                            player.setSource(Uri.parse(data.getMedia()));

                        }else{
                            //videoView.setVisibility(View.GONE);
                            ImageView imageView = new ImageView(getApplicationContext());
                            Picasso.with(getApplicationContext()).load(data.getImage()).into(imageView);
                            mediaTop.addView(imageView);
                            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                        }
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                            content.setText(Html.fromHtml(data.getContent(), Html.FROM_HTML_MODE_LEGACY));
                        }else{
                            content.setText(Html.fromHtml(data.getContent()));
                        }
                        title.setText(data.getTitle());
                        if(data.getRealReason()){
                            if(getIntent().getBooleanExtra("showReason",false)){
                                container_goal.setVisibility(View.VISIBLE);
                            }else{
                                container_goal.setVisibility(View.GONE);
                            }
                        }else{
                            container_goal.setVisibility(View.GONE);
                        }
                        break;

                    case 404:
                        finish();
                        break;

                    default:
                        MaterialDialog.Builder dialog = createMessage();
                        dialog.build();
                        dialog.show();
                        break;
                }
                dialog.dismiss();
            }

            @Override
            public void onFailure(Call<ViewSingleArticle> call, Throwable t) {
                dialog.dismiss();
                MaterialDialog.Builder dialog = createMessage();
                dialog.build();
                dialog.show();
            }
        });
    }

    private MaterialDialog.Builder createMessage(){
        return new MaterialDialog.Builder(this)
                .title("Error")
                .content("Server not available, try again later")
                .positiveText("Accept");
    }

    private MaterialDialog loader(){
        MaterialDialog.Builder builder = new MaterialDialog.Builder(this)
                .content("Please wait...")
                .progress(true, 0);
        return builder.build();
    }

    @Override
    protected void onPause() {
        super.onPause();
        player.pause();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onStarted(BetterVideoPlayer player) {

    }

    @Override
    public void onPaused(BetterVideoPlayer player) {

    }

    @Override
    public void onPreparing(BetterVideoPlayer player) {

    }

    @Override
    public void onPrepared(BetterVideoPlayer player) {

    }

    @Override
    public void onBuffering(int percent) {

    }

    @Override
    public void onError(BetterVideoPlayer player, Exception e) {

    }

    @Override
    public void onCompletion(BetterVideoPlayer player) {

    }

    @Override
    public void onToggleControls(BetterVideoPlayer player, boolean isShowing) {

    }

    private void  setGoal(String msg){
        dialogGetVideo = loader();
        dialogGetVideo.show();
        // Create the call of the service view program progress
        Call<ResponseBody> call = services.set_goal(user.getId(), msg);

        // Executing call of the service time to start
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                switch (response.code()) {
                    case 200:
                        Intent i = new Intent(getApplicationContext(), SplashScreen.class);
                        startActivity(i);
                        finish();
                        break;
                    default:
                        break;
                }
                dialogGetVideo.dismiss();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                //setGoalProgress.setVisibility(View.GONE);
                createToast("Error" + t);
                dialogGetVideo.dismiss();
            }
        });
    }

    private void createToast(String msg){
        Toast toast = Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

}
